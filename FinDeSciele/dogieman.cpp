#include <SDL/SDL.h>
#include <list>
#include <iostream>
#include <math.h>
#include "defines.h"
#include "dogieman.h"
#include "policeman.h"
#include "bdata.h"
#include "colisions.h"
#include "colisives.h"
#include "dynamic.h"
#include "heroe.h"
#include "dog.h"
#include "corpse.h"

#define DOGIEMAN_DELAY 15
#define DOGIEMAN_MAX 20
#define DOGIEMAN_POWER 4

using namespace std;

BDATA dogieman::me("pdata/dogieman/dogieman.dat");

extern heroe *player;
extern list<object*> colision_makers;
extern list<dynamic*> dynamics;	
extern list<colisives*> colisioning;

dogieman::dogieman(int _x,int _y):policeman(_x,_y){
	current_frame = me.get_surface(frame);
	my_dog = new dog(x-30,y+48,this);
	dynamics.push_front(my_dog);
	colision_makers.push_back(my_dog);
	colisioning.push_back(my_dog);
}

dogieman::~dogieman(){
	if(my_dog){ 
		my_dog->unleache();
		my_dog->my_owner_died();
	}
}

void dogieman::take_action(){
	if(did_my_job) return;
	action = 50;
	did_my_job = true;
	my_dog->unleache();
	my_dog = 0;
	player->give_score(150);
}

bool dogieman::live(){
	object* tmp = 0;
	
	if(my_colisions->has_with(KNIFE)){
		corpse *nc;
		if(direction < 0) nc = new corpse(x,y,0);
		else nc = new corpse(x,y,1);
		colision_makers.push_back(nc);
		colisioning.push_back(nc);
		dynamics.push_front(nc);
		player->give_score(1000);
		return false;
	}
	
	hvct = direction*DOGIEMAN_POWER;
	if(tmp = my_colisions->has_with(BARRIER,TOP)){
		if((x < tmp->get_x() && direction == LEFT) ||
		  (get_xx() > tmp->get_xx() && direction == RIGHT)){
			direction = -direction;	
			hvct = -hvct;
		}
	}
	if(my_colisions->has_with(BARRIER,EDGE_L) || my_colisions->has_with(BARRIER,EDGE_R)){
		direction = -direction; 
		hvct  = -hvct;
	}
	if(!my_colisions->has_with(BARRIER,TOP)) vvct += GRAVITY;
	else vvct = 0;
	
	if(found_sth()){
		if(!did_my_job || action) hvct = 0;
	}
	else if(action) hvct = 0;
	else if(fabs(get_yy() - player->get_yy()) < HEIGTH_BORDER && 
		fabs(x - player->get_x()) < HEAR_BORDER)
		direction = -direction;
	else if(rand() % 1000 == 999) direction = -direction; 

	if(vvct > DOGIEMAN_MAX) vvct = DOGIEMAN_MAX;
	
	if(my_dog) my_dog->set_dir(direction);
	
	animate();
	move(hvct,vvct);
	my_colisions->reset();
	
 	return true;
}
	
void dogieman::animate(){

	if(action > 0){
		--action;
		if(direction < 0) frame = 3;
		else frame = 7;
		current_frame = me.get_surface(frame);
		return;
	}
	else if(action < 0){
		action++;
		if(direction < 0) frame  = 8;
		else frame = 9;
		current_frame = me.get_surface(frame);
		return;
	}

	if(vvct){
		if(direction < 0) frame = 0;
		else frame = 4;
		current_frame = me.get_surface(frame);
		return;
	}
	if(hvct == 0){
		if(direction < 0) frame = 0;
		else frame = 4;
		current_frame = me.get_surface(frame);
		return;
	}	
	
	if(time++ < DOGIEMAN_DELAY) return;
	if(hvct < 0){
		time = 0;
		if(frame != 1) frame = 1;
		else frame = 2;
	}
	else{
		time = 0;
		if(frame != 5) frame = 5;
		else frame = 6;
	}

	
	current_frame = me.get_surface(frame);

}
