#include <iostream>
#include <cstring>

using namespace std;

bool palindrom(string);

int main() {
	int choice;
	
	while(choice != 2)
	{
		cout <<"\n1. Sprawdz czy palindrom\n2. Wyjscie ";
		cin >> choice;
	
		if(choice == 1) {
			cout << "Podaj wyraz ";
			string str;
			cin >> str;
			if(palindrom(str)) cout << "\n To palindrom";
			else cout << "\nTo NIE palindrom";
		}	
	}
	return 0;
}

bool palindrom(string str) {
	bool result = true;
	
	int length = str.length();
	
	for(int i=0; result && i<length/2; i++) {
		if(str[i] != str[length-i-1]) result = false;	
	}

	return result;
}
