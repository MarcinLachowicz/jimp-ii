//klasa dla obiektów animujących mruganie kursora textowego
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#ifndef CURSOR_H
#define CURSOR_H
class cursor{
	public:
		cursor(SDL_Surface*,SDL_Surface*,TTF_Font* ,SDL_Color,char* ="_");
		~cursor(){delete type;}
		
		void show(SDL_Rect&);
		void hide(SDL_Rect&);
	private:
		SDL_Surface* bground;
		SDL_Surface* screen;
		TTF_Font* font;
		SDL_Color color;
		char* type;
};
#endif
