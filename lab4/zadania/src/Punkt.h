/*
 * Punkt.h
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#ifndef PUNKT_H_
#define PUNKT_H_

class Punkt {
private:
	double x, y;
public:
	//Konstruktor bezparametrowy
	Punkt();
	//Konstruktor parametrowy
	Punkt(double _x, double _y);
	//Konstruktor kopiujący
	Punkt(const Punkt&);
	//Destruktor
	~Punkt();

	double distance(Punkt inny);
	void wyswietl();

	// Krótkie funkcje mogą być zdefiniowane w pliku
	// nagłówkowym i będą traktowane jako funkcje inline
	double getX() {
		return x;
	}
	double getY() {
		return y;
	}
	void setX(double _x) {
		x = _x;
	}
	void setY(double _y) {
		y = _y;
	}
};

#endif /* PUNKT_H_ */
