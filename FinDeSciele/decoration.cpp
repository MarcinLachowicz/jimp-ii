#include <SDL/SDL.h>
#include <list>
#include "object.h"
#include "bdata.h"
#include "invisible_ground.h"
#include "decoration.h"

using namespace std;

BDATA decoration::me("ddata/decoration.dat",255,255,255);
extern list<object*> colision_makers;

decoration::decoration(int _x, int _y, int pattern){
	x = _x;
	y = _y;
	current_frame = me.get_surface(pattern);
	give_local_barierre(pattern);
}

void decoration::draw(SDL_Surface* dest){
	SDL_Rect dest_map;
	
	dest_map.x = x;
	dest_map.y = y;
	
	SDL_BlitSurface(current_frame,NULL,dest,&dest_map);
}

void decoration::give_local_barierre(int pattern){
	switch(pattern){
		case 4: colision_makers.push_back(new invisible_ground(x+94,y+208,x+182,y+231));
			break;
		case 6: colision_makers.push_back(new invisible_ground(x+40,y+171,x+119,y+190));
			colision_makers.push_back(new invisible_ground(x+158,y+166,x+237,y+185));
			break;
		case 7: colision_makers.push_back(new invisible_ground(x+97,y+109,x+167,y+127));
			colision_makers.push_back(new invisible_ground(x+61,y+237,x+136,y+254));
			colision_makers.push_back(new invisible_ground(x+159,y+234,x+234,y+251));
			break;
		case 8: colision_makers.push_back(new invisible_ground(x+32,y+243,x+93,y+256));
			colision_makers.push_back(new invisible_ground(x+132,y+243,x+194,y+256));
			break;
		case 9: colision_makers.push_back(new invisible_ground(x+45,y+132,x+127,y+151));
			colision_makers.push_back(new invisible_ground(x+169,y+131,x+251,y+150));
			break;

	}
}
