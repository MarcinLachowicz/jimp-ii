#include <iostream>

using namespace std;

int silnia(int);

int main() {
	int a;
	
	cout << "Podaj liczbe: "; 	
	cin >> a;
	cout << "Silnia: " << silnia(a) << endl;

	return 0;
}

/*int silnia(int a) {
	int result = 1;

	if (a > 0) {
		for(int i=1; i<=a; i++) {
			result *= i;
		}
	}


	return result;
}*/

int silnia(int a) {
	int result = 1;

	if(a>0) {
		result = a * silnia(a-1);
	}
	else result = 1;

	return result;
}
