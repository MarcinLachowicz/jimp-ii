/*
 * Trojkat.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef TROJKAT_H_
#define TROJKAT_H_

#include "Ksztalt.h"

class Trojkat: public Ksztalt {
	public:
		Trojkat();
		virtual ~Trojkat();
		virtual void rysuj();
};

#endif /* TROJKAT_H_ */
