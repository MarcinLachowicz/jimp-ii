//klasa dla obiekt�w b�d�cych policjantami z pa�kami.
#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef POLICEMAN_H
#define POLICEMAN_H
class policeman: public colisives{
		 public:
		 	policeman(int,int,bool=false);
			virtual ~policeman();
			
			virtual bool live();
			virtual void draw(SDL_Surface* dest){object::draw(dest);}
			virtual void find_colisions();
		 protected:
		 	virtual bool see_him(dynamic*);
			virtual bool found_sth();
			virtual double f1(int ex);
			virtual double f2(int ex);
			virtual void animate();
		 	void throw_billy();
			virtual void take_action();
			
			int hvct,vvct,direction;
			int reloading;
			int time;
			int action;
			bool did_my_job;
		 private:
		 	static BDATA me;
};
				
#endif 
