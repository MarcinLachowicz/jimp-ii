/*
 * Storey.h
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#ifndef STOREY_H_
#define STOREY_H_

#define BLUE_TYPE 0
#define YELLOW_TYPE 1
#define PURPLE_TYPE 2

#include <vector>
#include <cstdlib>
#include "Brick.h"

class Storey {
public:
	Storey(int);
	virtual ~Storey();
	Brick* getBrick(int);

	void setType(int);
	int getType();

	int getSize();
	int countEmptyBricks();
	int countNonReperitiveBricks();

	bool isFilled();

	//TODO is filled correctly
	bool isFilledCorrectly();
	bool isAllowedValue(int);

private:
	int size;
	std::vector<Brick> bricks;
	int type;
};

#endif /* STOREY_H_ */
