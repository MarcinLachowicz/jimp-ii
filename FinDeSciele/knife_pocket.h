//paczka no�y kt�re bohater moze zebrac
#include <SDL/SDL.h>
#include "bdata.h"
#include "colisives.h"

#ifndef KNIFE_POCKET_H
#define KNIFE_POCKET_H
class knife_pocket: public colisives{
	public:
		knife_pocket(int,int,int);
		virtual ~knife_pocket(){}
		
		bool live();
		void draw(SDL_Surface* dest){object::draw(dest);}
		void find_colisions();
	protected:
		int knives;
		bool taken;
	private:
		static BDATA me;
};
#endif
 
