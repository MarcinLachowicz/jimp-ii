//klasa dla obiektów wyświetlajacych zwłoki
#include <SDL/SDL.h>
#include "defines.h"
#include "colisives.h" 
#include "bdata.h"

#ifndef CORPSE_H
#define CORPSE_H
class corpse: public colisives{
	public:
		corpse(int,int,int);
		virtual ~corpse();
		
		bool live();
		void draw(SDL_Surface* dest);
		void animate();
		bool is_seen(){return seen;}
		void find_colisions();
		void see_you(){type=UNKNOWN;}
	protected:
		int vvct;
		bool seen;
	private:
		static BDATA me;
};
#endif
