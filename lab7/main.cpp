/*
 * main.cpp
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#include <iostream>
#include "headers/Punkt.h"
#include "headers/Punkt3d.h"
#include "headers/Kula.h"

using namespace std;

int main(int argc, char **argv) {
	Punkt p2;
	Punkt3d p3(1, 1, 1);
	cout << p2.distance(p3) << endl;
	cout << p3 << endl;

	//----------------------------
	Kula k(0,0,0,10);

	cout << "Pole przekroju: " << k.Kolo::pole() << endl;
	return 0;
}

