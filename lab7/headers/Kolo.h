/*
 * Kolo.h
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#ifndef KOLO_H_
#define KOLO_H_
#define PI 3.14
class Kolo {
	public:
		Kolo(double, double, double);
		virtual ~Kolo();

		double pole();
	protected:
		double x, y, r;
};

#endif /* KOLO_H_ */
