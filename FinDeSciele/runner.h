#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef RUNNER_H
#define RUNNER_H
class runner: public colisives{
		 public:
		 	runner(int,int,bool=false);
			virtual ~runner();
			
			virtual bool live();
			virtual void draw(SDL_Surface* dest){object::draw(dest);}
			virtual void find_colisions();
		 protected:
		 	virtual bool see_him(dynamic*);
			virtual bool found_sth();
			virtual double f1(int ex);
			virtual double f2(int ex);
			virtual void animate();
		 	void start_sprint();
			virtual void take_action();
			
			int hvct,vvct,direction;
			int time;
			int action;
			bool did_my_job;
		 private:
		 	static BDATA me;
		 	double speed_factor;
};
				
#endif 
