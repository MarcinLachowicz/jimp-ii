/*
 * Marsjanin.h
 *
 *  Created on: 09-03-2013
 *      Author: marcin
 */

#ifndef MARSJANIN_H_
#define MARSJANIN_H_

#include <iostream>

class Marsjanin {
public:
	static int counter;
	Marsjanin();
	virtual ~Marsjanin();

	void atakuj();
};

#endif /* MARSJANIN_H_ */
