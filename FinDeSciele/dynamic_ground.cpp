#include <SDL/SDL.h>
#include "dynamic.h"
#include "defines.h"
#include "bdata.h"
#include "dynamic_ground.h"

#define MAXX_AMPLITUDE 10
#define MAXY_AMPLITUDE 10

BDATA dynamic_ground::me("brdata/barrier.dat");

dynamic_ground::dynamic_ground(int _x, int _y, int pattern, int x_amp, int y_amp)
:x_amplitude(x_amp),y_amplitude(y_amp),x_direction(LEFT),y_direction(UP){
	x = _x += x_amplitude;
	y = _y += y_amplitude;
	current_frame = me.get_surface(pattern);
	
	type = BARRIER;
}

bool dynamic_ground::live(){
	if(y_direction == DOWN && y_amplitude == MAXY_AMPLITUDE) y_direction = UP;
	else if(y_direction == UP && y_amplitude == -MAXY_AMPLITUDE ) y_direction = DOWN;

	if(y_direction == DOWN){ y++; y_amplitude++;}
	else {y--;y_amplitude--;}
	
	return true;

}
