//heroe - czyli klasa dla g��wnego bohatera
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "defines.h"
#include "bdata.h"
#include "colisives.h"

#ifndef HEROE_H
#define HEROE_H
class heroe:public colisives{
	public:
		heroe(int,int);
		virtual ~heroe();
		
		virtual void find_colisions();
		virtual bool live();
		virtual void animate();
		virtual void draw(SDL_Surface* dest);
		virtual bool is_seen(){return is_in_the_light;}
		
		int get_h_vct(){return hvct;}
		int get_v_vct(){return vvct;}
		int get_dir(){return direction;}
		void give_score(int ds){scores+=ds;}
		void give_ammo(int da){knives+=da;}
		void give_life(){if(lifes < 3) lifes++;}
		int get_status(){return status;}
		int get_scores(){return scores;}
		void correct();
	protected:
		void speed_control();
		void throw_knife();
		
		int reloading;
		int knives;
		int direction;
		int hvct;
		int vvct;
		int lifes;
		int scores;
		int godmode;
		bool is_in_the_light;
		int status;
	private:
		static BDATA me;
		
		class panel{
			public:
				panel(){my_font = TTF_OpenFont("gdata/betad.ttf",MIN_FONT);}
				~panel(){TTF_CloseFont(my_font);}
				void draw(SDL_Surface*,int,int,int);
			private:
				TTF_Font* my_font;
		}Panel;
				
};

#endif

