/*
 * Punkt.cpp
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#include "../Punkt.h"

#include <math.h>
#include <iostream>

using namespace std;

/* Aby wskazać, ze definicja funkcji dotyczy metody danej klasy
 stosujemy tzw. operator zasięgu - "::"
 */

//Specjalna inicjalizacja zmiennych. Zmienne są inicjowane
//nim zostanie wywołane ciało konstruktora
Punkt::Punkt() :
		x(0), y(0) {
	cout << "Konstruktor bezparametrowy" << endl;
}

Punkt::Punkt(double _x, double _y) {
	cout << "Konstruktor parametrowy" << endl;
	x = _x;
	y = _y;
}

Punkt::Punkt(const Punkt& p) {
	this->x = p.x;
	this->y = p.y;
	cout << "Konstruktor kopiujący";
}

Punkt::~Punkt() {
	cout << "Destruktor! Nic nie robie, bo nie musze zwalniać pamięci!";
	cout << endl;
}

double Punkt::distance(Punkt inny) {
	return sqrt(pow(x - inny.x, 2) + pow(y - inny.y, 2));
}

void Punkt::wyswietl() {
	cout << "(" << x << ";" << y << ")";
}

