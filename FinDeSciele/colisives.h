//colisives to klasa obiekt�w dla kt�rych badanie kolizji z innymi obiektami jetst wa�ne.
#include <SDL/SDL.h>
#include "dynamic.h"

#ifndef COLISIVES_H
#define COLISIVES_H

class colisions;
class colisives:public dynamic{
	friend class colisions;
	public:
		virtual ~colisives(){}
		virtual void find_colisions() = 0;
		virtual void set_colision(object*,int,int);
	protected:
		colisions* my_colisions;
};

#endif
