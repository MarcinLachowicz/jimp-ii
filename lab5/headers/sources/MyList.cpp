/*
 * MyList.cpp
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */

#include "../MyList.h"

MyList::MyList() :
		head(NULL) {

}

MyList::MyList(const MyList& ml) : head(NULL) {
	for(int i=0; i<ml.getLength(); i++) {
		Node* temp(ml.getNode(i));
		if(temp != NULL) this->pushBack(temp->getData());
	}
}

MyList::~MyList() {
	for(int i=0; i<getLength(); i++) {
		popBack();
	}
}

int MyList::getLength() const {
	int counter = 0;

	Node* temp = head;

	while (temp != NULL) {
		counter++;
		temp = temp->getNext();
	}

	return counter;
}

void MyList::pushFront(string _s) {
	Node* temp = new Node(_s, head);
	head = temp;
}

void MyList::pushBack(string _s) {
	if (head == NULL) {
		head = new Node(_s, NULL);
	} else {
		getNode(getLength()-1)->setNext(new Node(_s, NULL));
	}
}


void MyList::popFront() {
	if (head != NULL) {
		Node* temp = head->getNext();
		delete head;
		head = temp;
	}
}

void MyList::popBack() {
	int listLength = getLength();
	if(listLength == 1){
		delete head;
		head = NULL;
	} else if(listLength > 1) {
		Node* temp = getNode(getLength()-2);
		delete temp->getNext();
		temp->setNext(NULL);
	}

}


void MyList::setNode(string _s, int n) {
	Node* temp = getNode(n);
	if(temp != NULL) {
		temp->setData(_s);
	}
}

Node* MyList::getNode(int n) const {
	Node* result = NULL;
	Node* temp = head;
	if ((temp != NULL) && (n >= 0)) {
		for (int i = 0; (i != n) && temp != NULL; i++) {
			temp = temp->getNext();
		}
		result = temp;
	}
	return result;
}

void MyList::print() {
	Node* temp = head;

	while(temp != NULL) {
		std::cout << temp->getData() << std::endl;
		temp = temp->getNext();
	}
}
