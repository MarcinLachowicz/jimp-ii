//funkcje do wczytywania obiekt�w z pliku i�adowania ich do pami�ci
#include <iostream>
#include <fstream>
#include <list>
#include "object.h"
#include "bdata.h"
#include "decoration.h"
#include "dogieman.h"
#include "heroe.h"
#include "vscreen.h"
#include "dog.h"
#include "policeman.h"
#include "runner.h"
#include "visible_ground.h"
#include "invisible_ground.h"
#include "dynamic_ground.h"
#include "knife_pocket.h"
#include "light.h"
#include "water.h"
#include "house.h"
#include "life.h"

using namespace std;



vscreen* read_vscreen(ifstream& input);
policeman* read_policeman(ifstream& input);
runner* read_runner(ifstream& input);
dogieman* read_dogieman(ifstream& input);
heroe* read_heroe(ifstream& input);
visible_ground* read_visible_ground(ifstream& input);
dynamic_ground* read_dynamic_ground(ifstream& input);
invisible_ground* read_unvisible_ground(ifstream& input);
decoration* read_decoration(ifstream& input);
light* read_light(ifstream& input);
water* read_water(ifstream& input);
house* read_house(ifstream& input);
knife_pocket* read_knife_pocket(ifstream& input);
dog* read_dog(ifstream& input);
life* read_life(ifstream& input);

bool load(const char* filename);
void unload();
void load_config(int &,int&);
void write_config(int intr, int fscr);
