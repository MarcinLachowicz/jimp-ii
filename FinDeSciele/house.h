//house to klasa dla obiektu bedacego domem głównego bohatera
//kolizja z house jest równoważna ukończeniu gry
#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef HAUSE_H
#define HAUSE_H
class house:public colisives{
	public:
		house(int,int);
		virtual ~house();
		
		virtual bool live();
		virtual void draw(SDL_Surface*);
		virtual void find_colisions();
	protected:
		bool heroe_is_in;
	private:
		static BDATA me;
};
#endif
		

