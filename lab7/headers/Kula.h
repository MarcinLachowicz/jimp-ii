/*
 * Kula.h
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#ifndef KULA_H_
#define KULA_H_

#include "Kolo.h"

class Kula : public Kolo {
	protected:
		double z;
	public:
		Kula(double, double, double, double);

		double pole();
		virtual ~Kula();
};

#endif /* KULA_H_ */
