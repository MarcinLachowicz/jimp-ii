/*
 * main.cpp
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#include "src/Punkt.h"
#include "src/Kwadrat.h"
#include "src/Zespolona.h"
#include "src/DTab.h"

#include <iostream>

using namespace std;

int main(int argc, char **argv) {
	Zespolona c(1, 1);
	Zespolona d(2, 3);
	c.print();
	c.add(d);
	cout << endl;
	c.print();
	d.pow(3);
	cout << endl;
	d.print();

	DTab tab(1);
	tab.add(10);
	tab.add(11);
	cout << endl;
	tab.print();

	Kwadrat k(Punkt(0,0), Punkt(0,4), Punkt(4,4), Punkt(4,0));
	cout << endl << "Obwod: " << k.getPerimeter() << "  Pole: " << k.getSurface();

	return 0;
}




