/*
 * main.cpp
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */

#include <iostream>
#include <list>
#include <cstdlib>

#include "headers/Marsjanin.h"

using namespace std;

int main(int argc, char **argv) {
	list<Marsjanin*> lista;

	srand(time(NULL));
	while (1) {
		if (rand() % 2) {
			lista.push_back(new Marsjanin);
		} else {
			if (Marsjanin::counter > 0) {
				Marsjanin* tmp = lista.back();
				lista.pop_back();
				delete tmp;
			}
		}
		if (Marsjanin::counter > 5) {
			for (list<Marsjanin*>::iterator i = lista.begin(); i != lista.end();
					i++) {
				(*i)->atakuj();
			}
		}
	}
	return 0;
}

