################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../headers/sources/Kolo.cpp \
../headers/sources/Kula.cpp \
../headers/sources/Punkt.cpp \
../headers/sources/Punkt3d.cpp 

OBJS += \
./headers/sources/Kolo.o \
./headers/sources/Kula.o \
./headers/sources/Punkt.o \
./headers/sources/Punkt3d.o 

CPP_DEPS += \
./headers/sources/Kolo.d \
./headers/sources/Kula.d \
./headers/sources/Punkt.d \
./headers/sources/Punkt3d.d 


# Each subdirectory must supply rules for building sources it contributes
headers/sources/%.o: ../headers/sources/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


