/*
 * Pracownik.cpp
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#include "../Pracownik.h"

Pracownik::Pracownik(string i, string n, string p, Umowa* u) :
		imie(i), nazwisko(n), pesel(p), umowa(u) {

}

Pracownik::~Pracownik() {
	// TODO Auto-generated destructor stub
}

Pracownik::Pracownik(const Pracownik& p) {
	imie = p.imie;
	nazwisko = p.nazwisko;
	pesel = p.pesel;
	umowa = p.umowa;
}

double Pracownik::pobierzPensje() {
	return umowa->pobierzNetto();
}

ostream& operator<<(ostream& stream, Pracownik& p) {
	cout << "Imie: " << p.imie << endl << "Nazwisko: " << p.nazwisko << endl;
	cout << "Pesel: " << p.pesel << endl << "Pensja netto: " << p.pobierzPensje() << endl;
	return stream;
}
