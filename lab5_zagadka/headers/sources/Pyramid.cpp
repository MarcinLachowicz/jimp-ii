/*
 * Pyramid.cpp
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#include "../Pyramid.h"

Pyramid::Pyramid(int _heigth) {
	heigth = _heigth;
	for(int i = 0; i < _heigth; i++) {
		storeys.push_back(Storey(i + 1));
	}
}

Pyramid::~Pyramid() {
	// TODO Auto-generated destructor stub
}

Storey* Pyramid::getStorey(int _n) {
	return &storeys[_n];
}

void Pyramid::print() {
	int bottomStoreySize = getStorey(heigth - 1)->getSize();

	for(int i = 0; i < heigth; i++) {
		int currentStoreySize = getStorey(i)->getSize();
		for(int k = 0; k < ((bottomStoreySize) - 1) - ((currentStoreySize) - 1); k++) {
			std::cout << " ";
		}
		for(int j = 0; j < currentStoreySize; j++) {
			std::cout << storeys[i].getBrick(j)->getContent() << " ";
		}
		std::cout << std::endl;
	}
}

void Pyramid::solve() {
	bool found = false;

	for(int i = heigth - 1; i > 0 && !found; i--) {
		Storey* currentStorey = getStorey(i);
		Storey* nextStorey = getStorey(i - 1);
		for(int j = 0; j < currentStorey->getSize() - 1 && !found; j++) {
			Brick* currentBrick = currentStorey->getBrick(j);
			Brick* nextBrick = currentStorey->getBrick(j + 1);
			Brick* childBrick = nextStorey->getBrick(j);

			if(countFilledBricks(currentBrick, nextBrick, childBrick) > 0) {
				found = true;
				Brick* emptyBrick;
				Storey* startingStorey = currentStorey;
				if(!currentBrick->isSet())
					emptyBrick = currentBrick;
				else if(!nextBrick->isSet())
					emptyBrick = nextBrick;
				else if(!childBrick->isSet()) {
					emptyBrick = childBrick;
					startingStorey = nextStorey;
				}
				bool flag = false;
				for(int k = 1; k < 10 && !flag; k++) {
					if(startingStorey->isAllowedValue(k)) {
						emptyBrick->setContent(k);
						flag = insert();
						if(!flag)
							emptyBrick->setContent(0);
					}
				}
			}
		}
	}
}

int Pyramid::countFilledBricks(Brick* _a, Brick* _b, Brick* _c) {
	int counter = 0;

	if(_a->isSet())
		counter++;
	if(_b->isSet())
		counter++;
	if(_c->isSet())
		counter++;

	return counter;
}

bool Pyramid::insert() {
	bool flag = false;

	flag = isWholeFilled();

	if(!flag) {
		Brick* currentBrick, *nextBrick, *childBrick;
		Storey* currentStorey;
		Storey* nextStorey;
		bool flag2 = false;
		for(int i = heigth - 1; (i > 0) && !flag2; i--) {
			currentStorey = getStorey(i);
			nextStorey = getStorey(i - 1);
			for(int j = 0; (j < currentStorey->getSize() - 1) && !flag2; j++) {
				currentBrick = currentStorey->getBrick(j);
				childBrick = nextStorey->getBrick(j);
				nextBrick = currentStorey->getBrick(j + 1);

				if(countFilledBricks(currentBrick, nextBrick, childBrick) == 2 ) {
					flag2 = true;
				}
			}
		}

		if(flag2) {
			Brick* emptyBrick, *filledBrickA, *filledBrickB;
			Storey *emptyStorey;
			if(!currentBrick->isSet()) {
				emptyStorey = currentStorey;
				emptyBrick = currentBrick;
				filledBrickA = nextBrick;
				filledBrickB = childBrick;
			} else if(!nextBrick->isSet()) {
				emptyStorey = currentStorey;
				emptyBrick = nextBrick;
				filledBrickA = currentBrick;
				filledBrickB = childBrick;
			} else if(!childBrick->isSet()) {
				emptyStorey = nextStorey;
				emptyBrick = childBrick;
				filledBrickA = nextBrick;
				filledBrickB = currentBrick;
			}

			int value = filledBrickA->getContent() + filledBrickB->getContent();

			if(emptyStorey->isAllowedValue(value) && !flag) {
				emptyBrick->setContent(value);
				flag = insert();
				if(!flag)
					emptyBrick->setContent(0);
			}
			value = filledBrickA->getContent() - filledBrickB->getContent();
			if(emptyStorey->isAllowedValue(value) && !flag) {
				emptyBrick->setContent(value);
				flag = insert();
				if(!flag)
					emptyBrick->setContent(0);
			}
			value = filledBrickB->getContent() - filledBrickA->getContent();
			if(emptyStorey->isAllowedValue(value) && !flag) {
				emptyBrick->setContent(value);
				flag = insert();
				if(!flag)
					emptyBrick->setContent(0);
			}

		}
	}
	return flag;
}

bool Pyramid::isWholeFilled() {
	bool flag = true;
	for(int i = 0; i < heigth && flag; i++) {
		if(storeys[i].countEmptyBricks() != 0)
			flag = false;
	}
	return flag;
}

