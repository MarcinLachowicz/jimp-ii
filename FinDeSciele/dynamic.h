//dynamic to klasa bazowa dla wszyskich obiekt�w
//dynamic znaczy nie zawsze na ekranie, lub nie zawsze w tym samym miejscu
#include <SDL/SDL.h>
#include "object.h"

#ifndef DYNAMIC_H
#define DYNAMIC_H
class vscreen;
class dynamic:public object{
	public:
		virtual ~dynamic(){}
		virtual bool live() = 0;
	protected:
		int frame;
};
#endif
