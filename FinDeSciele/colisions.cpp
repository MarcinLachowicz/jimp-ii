#include <SDL/SDL.h>
#include <vector>
#include "defines.h"
#include "object.h"
#include "colisions.h"
#include "colisives.h"


#include <iostream>
using namespace std;


colisions::colisions(colisives* obj)
:main_obj(obj),x(obj->x),y(obj->y){
	reset();
}

int colisions::has_already(object* other_obj){
	vector<pocket>::iterator it = with_what.begin();
	for(it;it!=with_what.end();it++) if((*it).colided == other_obj) return (*it).type;
	
	return 0;
}

void colisions::reset(){
	with_what.erase(with_what.begin(),with_what.end());
}

void colisions::set(object* other_obj, int type, int side){
	with_what.push_back(pocket(other_obj,type,side));
}

bool colisions::has_any(){
	return !with_what.empty();
}
object* colisions::has_with(int type, int side){
	vector<pocket>::const_iterator it = with_what.begin();
	for(it;it != with_what.end();it++)
		if((*it).type == type && (*it).side == side) return (*it).colided;
	return NULL;
}

object* colisions::has_with(int type){
	vector<pocket>::const_iterator it = with_what.begin();
	for(it;it != with_what.end();it++)
		if((*it).type == type) return (*it).colided;
	return NULL;
}
	
	
bool colisions::just_say(object* other_obj){
	if(has_already(other_obj)) return true;;
	xx = main_obj->get_xx();
	yy = main_obj->get_yy();

	if(x < other_obj->get_x() && xx < other_obj->get_x() ||
	   xx > other_obj->get_xx() && x > other_obj->get_xx() ||
	   y < other_obj->get_y() && yy < other_obj->get_y() ||
	   yy > other_obj->get_yy() && y > other_obj->get_y())
		return false;
		
	return true;


}

int colisions::what_side(object* other_obj){
	xx = main_obj->get_xx();
	yy = main_obj->get_yy();
	int dx = other_obj->get_x(),
	    dy = other_obj->get_y(),
	    dxx = other_obj->get_xx(),
	    dyy = other_obj->get_yy();
	int side = 0;
	    
	//liczby dobrane tak, aby suma wskazywala jednoznacznie na to ktore pola byly aktywne
	    
	if(xx >= dx && xx <= dxx && yy <= dyy && yy >= dy) side+=2;
	if(xx >= dx && xx <= dxx && y <= dyy && y >= dy) side+=3;
	if(x >= dx && x <= dxx && yy <= dyy && yy >= dy) side+=4;
	if(x >= dx && x <= dxx && y <= dyy && y >= dy) side+=8;

	if(side){
		if(side == 6) side = TOP;
		else if(side == 11) side = BOTTOM;
		else if(side == 5) side = EDGE_L;
		else if(side == 12) side = EDGE_R;
		else if(side == 2 || side == 4){
			if(((yy-dy) < (dxx -x) && (yy-dy) < (xx - dx))) side=TOP;
			else if(side == 4) side = EDGE_R;
			else side = EDGE_L;
		}
		else if((dyy-y) < (dxx -x) && (dyy-y) < (xx - dx)) side = BOTTOM;
		else if(side == 8) side = EDGE_R;
		else side = EDGE_L;
		
		return side;
	}
		
	if(dx >= x && dxx <= xx ){
		if(dy <= yy && dy >= y) side = TOP;
		else if(dyy >= y && dyy <= yy) side = BOTTOM;
	}
	else if(dy >= y && dyy <= yy){
		if(dxx >= x && dxx <= xx) side = EDGE_R;
		else if(dx >= x && dx <= xx) side = EDGE_L;
	}
	return side;
}
	
	
void colisions::calculate(object* other_obj){
	if(has_already(other_obj)) return;
	int side,
	    type;
	colisives* temp;
	if(side = what_side(other_obj)){
		type = other_obj->is();
		with_what.push_back(pocket(other_obj,type,side));
		
		if(temp = dynamic_cast<colisives*>(other_obj))
			temp->set_colision(main_obj,main_obj->is(),-side);
		if(type == BARRIER)
			correction(other_obj,side);
	} 
}

void colisions::correction(object* obj, int side){
	switch(side){
		case TOP:
			main_obj->move(0,(obj)->get_y()-yy);	
			break;
		case BOTTOM:
			main_obj->move(0,(obj)->get_yy()-y);
			break;
		case EDGE_L:
			main_obj->move((obj)->get_x()-xx,0);
			break;
		case EDGE_R:
			main_obj->move((obj)->get_xx()-x,0);
			break;
		default:
			break;		
	}

}

		

