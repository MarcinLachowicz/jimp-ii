//vscreen.cpp
#include <SDL/SDL.h> 
#include <list>
#include <iostream>
#include "defines.h"
#include "object.h"
#include "bdata.h"
#include "vscreen.h"

using namespace std;

BDATA vscreen::me("vscrdata/vscr.dat");		// static variable for all vscreens

vscreen::vscreen(int _level=0){
	x=0;
	y=0;
	current_frame = me.get_surface(_level);
	sky = me.get_surface(_level+1);
}

inline void vscreen::set_level(int level){
current_frame = me.get_surface(level);
sky = me.get_surface(level+1);
}

void vscreen::move(int dx, int dy){
	x += dx;
	y += dy;
	
	if(has_reached_left_border()){ x = MIN_X;}
	else if(has_reached_right_border()){ x = current_frame->w - MAX_X;}
	if(has_reached_top_border()){ y = MIN_Y;}
	else if(has_reached_bottom_border()){ y = current_frame->h -MAX_Y;}
}
void vscreen::draw(SDL_Surface* dest) {
	SDL_Rect map;
	map.x = x;	
	map.w = MAX_X;
	map.y = y;
	map.h = MAX_Y;
	
	SDL_BlitSurface(sky,NULL,dest,NULL);
	SDL_BlitSurface(current_frame,&map,dest,NULL);
}
void vscreen::fill_with_statics(object* obj){
	obj->draw(current_frame);

}
void vscreen::fill_with_statics(SDL_Surface* bmp, SDL_Rect *map){
	SDL_BlitSurface(bmp,NULL,current_frame,map);
}
void vscreen::erase(SDL_Rect* map){
	SDL_FillRect(current_frame,map,SDL_MapRGB(current_frame->format, 255, 0, 255));
}



