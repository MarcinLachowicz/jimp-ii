//barera, grunt widoczne na ekranie
//beda "wmalowywane" w vscr;
#include <SDL/SDL.h>
#include "object.h"

#ifndef VISIBLE_GROUND_H
#define VISIBLE_GROUND_H
class BDATA;
class visible_ground:public object{
	public:
		visible_ground(int x, int y, int Xx, int Xy, int _frame);
		virtual ~visible_ground(){}
		
		virtual void draw(SDL_Surface*);
		virtual int get_xx(){return xx;}
		virtual int get_yy(){return yy;}
	protected:
		int xx, yy;
	private:
		static BDATA me;
};
#endif 
