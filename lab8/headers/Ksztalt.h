/*
 * Ksztalt.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef KSZTALT_H_
#define KSZTALT_H_

#include <iostream>
using namespace std;

class Ksztalt {
	public:
		Ksztalt();
		virtual ~Ksztalt();

		virtual void rysuj() = 0;
};

#endif /* KSZTALT_H_ */
