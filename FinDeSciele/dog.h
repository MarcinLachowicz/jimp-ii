//klasa dla obiekt�w b�d�cych psami policyjnymi. Pies moze wystepowac z policjantem,
//lub na wolnosci (pies nie wyst�puje w budzie;))
#include <SDL/SDL.h> 
#include "bdata.h"
#include "colisives.h"


#ifndef DOG_H
#define DOG_H
class dogieman;
class dog: public colisives{
	public:
		dog(int,int, dogieman* = 0);
		virtual ~dog();
		
		bool live();
		void find_colisions();
		void draw(SDL_Surface* dest){object::draw(dest);}
		
		void animate();
		void unleache(){unleached = true;}
		void set_dir(int dir){direction = dir;}
		void my_owner_died(){owner = 0;}
	protected:
		int vvct, hvct, direction,time;
		bool smells,unleached;
	private:
		static BDATA me;
		dogieman* owner;
};
#endif
