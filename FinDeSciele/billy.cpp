#include <SDL/SDL.h>
#include <list>
#include "billy.h"
#include "vscreen.h"
#include "colisions.h"
#include "defines.h"

#define BILLY_POWER 6
#define BILLY_RANGE 480
#define BILLY_DELAY 3

using namespace std;

BDATA billy::me("pdata/billy/billy.dat",255,255,255);
extern list<object*> colision_makers;

billy::billy(int _x,int _y,int dir)
:hit_something(false),distance(0),direction(dir),time(0){
	frame = 0;
	x = _x;
	y = _y;
	type = BULLET;
	
        hvct = direction*BILLY_POWER;
	my_colisions = new colisions(this);
	current_frame = me.get_surface(frame);
}
billy::~billy(){
	delete my_colisions;
}

void billy::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end()&&!hit_something;it++)
		if((*it)->is() == BARRIER || (*it)->is() == HEROE)
			hit_something = my_colisions->just_say(*it);
}

void billy::animate(){
	if(time++ < BILLY_DELAY) return;
	time = 0;
	if(direction > 0){
		if(frame == 0) 	frame = 1;
		else if(frame == 1) frame = 2;
		else if(frame == 2) frame = 3;
		else if(frame == 3) frame = 4;
		else if(frame == 4) frame = 5;
		else if(frame == 5) frame = 6;
		else frame = 0;
	}else{
		if(frame == 0) 	frame = 6;
		else if(frame == 6) frame = 5;
		else if(frame == 5) frame = 4;
		else if(frame == 4) frame = 3;
		else if(frame == 3) frame = 2;
		else if(frame == 2) frame = 1;
		else frame = 0;
	}
	
	current_frame = me.get_surface(frame);
}

bool billy::live(){
	if(hit_something) return false;
	if(distance > BILLY_RANGE) return false;
	
	distance += direction*hvct;
	animate();
	move(hvct,0);
	
	return true;
}
 
