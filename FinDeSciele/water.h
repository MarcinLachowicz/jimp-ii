//woda. bohater bedzie sie w niej topil
#include <SDL/SDL.h>
#include "dynamic.h"
#include "bdata.h"

#ifndef WATER_H
#define WATER_H
class water: public dynamic{
	public:
		water(int,int);
		virtual ~water(){}
		
		void draw(SDL_Surface* dest){object::draw(dest);}
		bool live();
		void animate();
	private:
		int time;
		static BDATA me;
};
#endif
		
