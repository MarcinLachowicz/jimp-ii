/*
 * Umowa.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef UMOWA_H_
#define UMOWA_H_

class Umowa {
	public:
	protected:
		double wynagrodzenieBrutto;
	public:
		Umowa(double pensja);

		virtual ~Umowa();
		virtual double pobierzNetto();
		double pobierzBrutto();

};

#endif /* UMOWA_H_ */
