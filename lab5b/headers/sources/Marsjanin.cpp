/*
 * Marsjanin.cpp
 *
 *  Created on: 09-03-2013
 *      Author: marcin
 */

#include "../Marsjanin.h"

int Marsjanin::counter = 0;

Marsjanin::Marsjanin() {
	counter++;
}

Marsjanin::~Marsjanin() {
	counter--;
}

void Marsjanin::atakuj() {
	std::cout << "Atakuje" << std::endl;
}

