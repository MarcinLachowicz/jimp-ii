#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <list>
#include "menus_gfx.h"
#include "defines.h"
#include "bdata.h"
#include "cursor.h"
using namespace std;

extern Uint8 *key;
extern int fullscreen_mode;



void wait(Uint32 time){
	Uint32 end = SDL_GetTicks()+time;
	while(SDL_GetTicks() < end);
}

void fade_out(SDL_Surface* screen, Uint8 step, int wait){
	SDL_Surface *black_sheet = SDL_CreateRGBSurface(SDL_HWSURFACE,640,480,8,0,0,0,0);
	Uint8 alpha = 0;
	
	if(Mix_PlayingMusic()) Mix_FadeOutMusic(step*wait);
	
	do{
		SDL_SetAlpha(black_sheet,SDL_SRCALPHA,alpha);
		SDL_BlitSurface(black_sheet,NULL,screen,NULL);
		SDL_Flip(screen);
		SDL_Delay(wait);
		alpha++;
	}while(alpha != step);
	
	while(Mix_PlayingMusic());
	
	SDL_FreeSurface(black_sheet);
	
}

void intro(SDL_Surface* screen){
	BDATA INTRO("gdata/intro/intro.dat");
	SDL_Surface* page = 0;
	Mix_Music* intro_music = Mix_LoadMUS("gdata/intro/intro_music.ogg");
	
	Mix_PlayMusic(intro_music,0);
	//dopoki wszystkie ramki nie zostana wyswietlone
	for(int frame = 0; frame < INTRO.get_fn(); frame++){
		page = INTRO.get_surface(frame);
		
		Uint8 alpha = 0;
		do{
			SDL_SetAlpha(page,SDL_SRCALPHA,alpha);
			SDL_BlitSurface(page,NULL,screen,NULL);
			SDL_Flip(screen);
			SDL_Delay(10);
			alpha++;
		}while(alpha != 255);
		SDL_Delay(20000);
	}
	
	fade_out(screen);
	if(intro_music) Mix_FreeMusic(intro_music);
}

void ir_blink(SDL_Surface* one, SDL_Surface* two,SDL_Surface* screen,int how_much){
	int blink_delay = 0; 
	int surface = 0;
	
	for(int counter = 0; counter < how_much;counter++){
		blink_delay = 50+rand() % (200+10*counter);
		SDL_Delay(blink_delay);
		if(surface == 0){ 
			SDL_BlitSurface(one,NULL,screen,NULL);
			surface = 1;
		}
		else{
			SDL_BlitSurface(two,NULL,screen,NULL);
			surface = 0;
		}
		SDL_Flip(screen);
	}
}
		

void madeit(SDL_Surface* screen){
	BDATA madeit("gdata/madeit/madeit.dat");
	int frame = 0;
	TTF_Font* text_font = TTF_OpenFont("gdata/betad.ttf",MID_FONT);
	SDL_Surface* text_surface;
	SDL_Color color = {0,0,0};
	SDL_Rect where_me = {150,MAX_Y/4};
	SDL_Rect where_he = {250,MAX_Y-MAX_Y/4};
	
	ir_blink(madeit.get_surface(0),madeit.get_surface(1),screen,10);
	text_surface = TTF_RenderText_Solid(text_font,"Michal Klimczyk",color);
	SDL_BlitSurface(text_surface,NULL,madeit.get_surface(1),&where_he);
	SDL_BlitSurface(text_surface,NULL,madeit.get_surface(2),&where_he);
	SDL_FreeSurface(text_surface);
	SDL_BlitSurface(madeit.get_surface(1),NULL,screen,NULL);
	SDL_Flip(screen);
	SDL_Delay(1000);
	
	ir_blink(madeit.get_surface(1),madeit.get_surface(2),screen,10);
	text_surface = TTF_RenderText_Solid(text_font,"Szymon Bobek",color);
	SDL_BlitSurface(text_surface,NULL,madeit.get_surface(2),&where_me);
	SDL_FreeSurface(text_surface);
	SDL_BlitSurface(madeit.get_surface(1),NULL,screen,NULL);
	SDL_BlitSurface(madeit.get_surface(2),NULL,screen,NULL);
	SDL_Flip(screen);
	SDL_Delay(5000);
}
	
	

string scan(SDL_Surface* screen,SDL_Surface *bg, SDL_Rect where){
	char text = ' ';
	char name[10] = "";
	int letters = -1,
	    time = 0,
	    w,h;
	SDL_Color color = {0,0,0};
	SDL_Surface* text_surface;
	TTF_Font* font= TTF_OpenFont("gdata/betad.ttf",MIN_FONT);
	cursor Cursor(screen,bg,font,color);
	
	while(key[SDLK_RETURN]) SDL_PumpEvents();
	
	time = SDL_GetTicks();
	while(!key[SDLK_RETURN]){
		SDL_PumpEvents();

		if((time + 500) > SDL_GetTicks()) Cursor.show(where);
		else if((time + 1000) > SDL_GetTicks()) Cursor.hide(where);
		else time = SDL_GetTicks();
	
		if(key[SDLK_a]) text='a';
		else if(key[SDLK_b]) text = 'b';
		else if(key[SDLK_c]) text = 'c';
		else if(key[SDLK_d]) text = 'd';
		else if(key[SDLK_e]) text = 'e';
		else if(key[SDLK_f]) text = 'f';
		else if(key[SDLK_g]) text = 'g';
		else if(key[SDLK_h]) text = 'h';
		else if(key[SDLK_i]) text = 'i';
		else if(key[SDLK_j]) text = 'j';
		else if(key[SDLK_k]) text = 'k';
		else if(key[SDLK_l]) text = 'l';
		else if(key[SDLK_m]) text = 'm';
		else if(key[SDLK_n]) text = 'n';
		else if(key[SDLK_o]) text = 'o';
		else if(key[SDLK_p]) text = 'p';
		else if(key[SDLK_r]) text = 'r';
		else if(key[SDLK_s]) text = 's';
		else if(key[SDLK_t]) text = 't';
		else if(key[SDLK_u]) text = 'u';
		else if(key[SDLK_w]) text = 'w';
		else if(key[SDLK_x]) text = 'x';
		else if(key[SDLK_y]) text = 'y';
		else if(key[SDLK_z]) text = 'z';
		else if(key[SDLK_BACKSPACE] && letters > -1){
			text = SDLK_BACKSPACE;
			char temp[2];
			temp[0] = name[letters];
			temp[1] = name[letters--] = '\0';
		
			Cursor.hide(where);
			
			TTF_SizeText(font,temp,&w,&h);
			where.x -= w;
			where.h = h;
			where.w = w;
			SDL_BlitSurface(bg,&where,screen,&where);
			
		}
		
		if(text != ' ' && text != SDLK_BACKSPACE && letters < 9){
			char temp[2];
			int w,h;
			
			name[++letters] = temp[0] = text;
			name[letters+1] = temp[1] = '\0';
			
			Cursor.hide(where);
			
			text_surface = TTF_RenderText_Solid(font,temp,color);
			SDL_BlitSurface(text_surface,NULL,screen,&where);
			
			TTF_SizeText(font,temp,&w,NULL);
			where.x += w;
		}
		
		SDL_Flip(screen);
		key[text] = 0;
		text = ' ';
		
	}
	Cursor.hide(where);
	TTF_CloseFont(font);
	
	return (string(name));
}


void scores(SDL_Surface* screen,int amount){
	SDL_Surface* background = SDL_LoadBMP("gdata/scores/scores.dat");
	SDL_Surface* text_surface;
	TTF_Font* score_font = TTF_OpenFont("gdata/betad.ttf",MIN_FONT);
	SDL_Color color = {0,0,0};
	SDL_Rect shown_map = {MAX_X/3,MAX_Y/5};
	SDL_Rect showp_map = {MAX_X-MAX_X/3,MAX_Y/5};
	SDL_Rect edit_map;
	string name;
	list<record> records;
	int n_rec = 0, score,h,w;
	
	fstream plik("gdata/scores/lscores.dat",ios::in);
	if(!plik){
		cout << "Error while reading scores...";
		return;
	}
	
	/*****************************EOF initialization**********************/
	
	SDL_BlitSurface(background,NULL,screen,NULL);
	SDL_Flip(screen);
	
	list<record>::iterator it = records.begin();
	while(plik >> name && plik >> score){
		records.push_back(record(name,score));
	}
	plik.close();
	score = amount;
	name = "";	
	it = records.begin();
	for(;it != records.end() && n_rec < 8;it++){	
		if((*it).score > score){
			record rec(name,score);
			it = records.insert(it,rec);
			break;
		}
		n_rec++;
	}
	
	it = records.begin();
	for(int counter = 0;it != records.end() && counter < 8;it++,counter++){
		if(counter > 0){
			shown_map.x = MAX_X/4;
			showp_map.x = MAX_X - MAX_X/4;
		}
		text_surface =TTF_RenderText_Solid(score_font,(*it).name.c_str(),color);
		SDL_BlitSurface(text_surface,NULL,screen,&shown_map);
		SDL_FreeSurface(text_surface);
		
		char c_score[15];
		sprintf(c_score,"%d",(*it).score);
		TTF_SizeText(score_font,c_score,&w,NULL);
		showp_map.x -= w;
		
		text_surface = TTF_RenderText_Solid(score_font,c_score,color);
		SDL_BlitSurface(text_surface,NULL,screen,&showp_map);
		SDL_FreeSurface(text_surface);
		
		TTF_SizeText(score_font,(*it).name.c_str(),NULL,&h);
		showp_map.y = shown_map.y += h;
	}
	SDL_Flip(screen);
	
	if(n_rec < 8){
		edit_map.y = MAX_Y/5 + n_rec*h;
		if(n_rec == 0) edit_map.x = MAX_X/3;
		else edit_map.x = MAX_X/4;
		
		name = scan(screen,background,edit_map);
		if(name == "") name = "anonim";
		
		it = records.begin();
		for(int counter = 0; counter != n_rec && it != records.end(); counter++,it++);
		(*it).name = name;
			
	}
	
	fstream plik_out("gdata/scores/lscores.dat",ios::out | ios::trunc);
	it = records.begin();
	for(n_rec = 0;n_rec < 8 && it != records.end();it++,n_rec++){
		plik_out << (*it).name << " " << (*it).score << endl;
	}
	plik.close();
	
	key[SDLK_RETURN] = 0;
	while(!key[SDLK_RETURN]) SDL_PumpEvents();
	
	TTF_CloseFont(score_font);
	SDL_FreeSurface(background);
}
		
	
	
		

int menu(SDL_Surface* screen){
	/*********************---= Declarations -=*******************************/
	BDATA menu("gdata/menu/menu.dat");
	TTF_Font* menu_font = TTF_OpenFont("gdata/betad.ttf",MIN_FONT);
	SDL_Color font_color= {0,0,0};
	SDL_Color choose_color= {240,12,29};
	SDL_Rect Squares[5];
	SDL_Surface* font_surface;
	char* words[] = {"Nowa Gra", "Opcje", "Scores", "Intro", "Exit"};
	const int MENU_DELAY = 150;
	int choose = 0,
	    frame = 0;
	bool free = true;
	Uint32 time = 0;
	
	/***********************---= End of declaration =-************************/    
	/***********************---= Prepear variables =-*************************/
	int w,h;
	for(int counter = 0; counter < 5; counter++){
		TTF_SizeText(menu_font,words[counter],&w,&h);
		Squares[counter].x = (MAX_X - w)/2;
		if(counter == 0) Squares[counter].y = 225;
		else Squares[counter].y = Squares[counter-1].y + Squares[counter-1].h;
		Squares[counter].w = w;
		Squares[counter].h = h;
	}
	/**********************---= End of Preparation =-*************************/
	/**********************---= Main Menu Loop =-*****************************/
	
	while(key[SDLK_RETURN]) SDL_PumpEvents();
	
	SDL_Event event;
	SDL_PollEvent(&event);
	while(event.type != SDL_QUIT && !key[SDLK_RETURN] && !key[SDLK_ESCAPE]){
		if(time+MENU_DELAY <= SDL_GetTicks()){
			SDL_BlitSurface(menu.get_surface(frame++),NULL,screen,NULL);
			time = SDL_GetTicks();
			if(frame == 8) frame = 0;
		}
		for(int counter = 0;counter < 5; counter++){
			if(choose == counter) font_surface = TTF_RenderText_Solid(menu_font,words[counter],choose_color);
			else font_surface = TTF_RenderText_Solid(menu_font,words[counter],font_color);
			SDL_BlitSurface(font_surface,NULL,screen,&Squares[counter]);
			SDL_FreeSurface(font_surface);
		}

		SDL_PumpEvents();
		if(free){
			if(key[SDLK_DOWN]){ 
				choose++;
				free =false;
			}
			else if(key[SDLK_UP]){
				choose--;
				free = false;
			}
			if(choose > 4) choose = 0;
			else if(choose < 0) choose = 4;
		}else if(!key[SDLK_UP] && !key[SDLK_DOWN]) free = true; 
		
		SDL_Flip(screen);
		SDL_PollEvent(&event);
		if(event.type == SDL_QUIT || key[SDLK_ESCAPE]) choose = 4;
	}
	/**********************---= End of Menu Loop =-*****************************/
	TTF_CloseFont(menu_font);
	return choose;
}

void options(SDL_Surface* screen){
	/*********************---= Declarations -=*******************************/
	BDATA menu("gdata/menu/menu.dat");
	BDATA options("gdata/options/options.dat");
	SDL_Surface* non_full = SDL_LoadBMP("gdata/options/nonfull.dat");
	TTF_Font* menu_font = TTF_OpenFont("gdata/betad.ttf",MIN_FONT);
	SDL_Color font_color= {0,0,0};
	SDL_Rect Squares[4];
	SDL_Surface* font_surface;
	char* words[] = {"Fullscreen", "On", "Off"};
	const int MENU_DELAY = 150;
	int frame = 0;
	bool free = true;
	int temp_fullscreen = fullscreen_mode;
	Uint32 time = 0;
	
	/***********************---= End of declaration =-************************/    
	/***********************---= Prepear variables =-*************************/
	int w,h;
	TTF_SizeText(menu_font,words[0],&w,&h);
	Squares[0].x = (MAX_X - w)/2;
	Squares[0].y = 225;
	Squares[1].y = Squares[2].y = Squares[0].y + 2*h;
	
	Squares[1].x = (MAX_X - w)/2 - 20;
	Squares[2].x = (MAX_X + w)/2 - 20;
	
	Squares[3].x = (MAX_X - options.get_surface(0)->w)/2;
	Squares[3].y = Squares[2].y+h;

	/**********************---= End of Preparation =-*************************/
	/**********************---= Main Menu Loop =-*****************************/
	
	while(key[SDLK_RETURN]) SDL_PumpEvents();
	SDL_Event event;
	SDL_PollEvent(&event);
	while(event.type != SDL_QUIT && !key[SDLK_RETURN] && !key[SDLK_ESCAPE]){
		if(time+MENU_DELAY <= SDL_GetTicks()){
			SDL_BlitSurface(menu.get_surface(frame++),NULL,screen,NULL);
			time = SDL_GetTicks();
			if(frame == 8) frame = 0;
		}
		for(int counter = 0;counter < 3; counter++){
			font_surface = TTF_RenderText_Solid(menu_font,words[counter],font_color);
			SDL_BlitSurface(font_surface,NULL,screen,&Squares[counter]);
			SDL_FreeSurface(font_surface);
		}

		SDL_PumpEvents();
		if(free){
			if(key[SDLK_RIGHT]){ 
				if(temp_fullscreen) temp_fullscreen = 0;
				free =false;
			}
			else if(key[SDLK_LEFT]){
				if(!temp_fullscreen) temp_fullscreen = 1;
				free = false;
			}
		}else if(!key[SDLK_LEFT] && !key[SDLK_RIGHT]) free = true;
		
		
		if(temp_fullscreen) SDL_BlitSurface(options.get_surface(0),NULL,screen,&Squares[3]);
		else SDL_BlitSurface(options.get_surface(1),NULL,screen,&Squares[3]);
		
		SDL_Flip(screen);
		SDL_PollEvent(&event);
	}
	/**********************---= End of Menu Loop =-*****************************/
	TTF_CloseFont(menu_font);
	if(fullscreen_mode != temp_fullscreen){ 
		Uint32 flags = SDL_HWSURFACE | SDL_DOUBLEBUF;
		fullscreen_mode = temp_fullscreen;
		SDL_FreeSurface(screen);
		if(fullscreen_mode) flags |= SDL_FULLSCREEN;
		SDL_Surface* screen = SDL_SetVideoMode(640,480,32,flags);
	}
}
