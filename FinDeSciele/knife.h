//klasa dla no�y - bohater pos�uguje si� tak� broni�
#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef KNIFE_H
#define KNIFE_H
class knife: public colisives{
	public:
		knife(int,int,int);
		virtual ~knife();
		
		bool live();
		void find_colisions();
		void draw(SDL_Surface* dest){object::draw(dest);}
		
	protected:
		int hvct;
		bool hit_something;
		int distance,
		    direction;
	private:
		static BDATA me;
};
#endif 
 
