/*
 * Storey.cpp
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#include "../Storey.h"

Storey::Storey(int _size) {
	size = _size;
	type = YELLOW_TYPE;
	for(int i = 0; i < size; i++) {
		bricks.push_back(Brick());
	}
}

Storey::~Storey() {
	// TODO Auto-generated destructor stub
}

Brick* Storey::getBrick(int _n) {
	return &bricks[_n];
}

void Storey::setType(int _type) {
	type = _type;
}

int Storey::getType() {
	return type;
}

bool Storey::isFilled() {
	bool flag = true;

	for(unsigned int i = 0; i < bricks.size() && flag; i++) {
		if(bricks[i].getContent() == 0)
			flag = false;
	}
	return flag;
}

int Storey::getSize() {
	return size;
}

bool Storey::isFilledCorrectly() {
	bool flag = true;

	switch (type) {
		case 0:
			for(int i = 0; i < size; i++) {
				int currentContent = bricks[i].getContent();
				for(int j = i + 1; j < size && flag; j++) {
					if(currentContent == bricks[j].getContent())
						flag = false;
				}
			}
			break;
		case 1:
			break;
		case 2:
			flag = false;
			for(int i = 0; i < size; i++) {
				int currentContent = bricks[i].getContent();
				for(int j = i + 1; j < size && !flag; j++) {
					if(currentContent == bricks[j].getContent())
						flag = true;
				}
			}
			break;
	}
	return flag;
}

bool Storey::isAllowedValue(int _value) {
	bool flag = true;

	if((_value < 1) || (_value > 9))
		flag = false;
	else {
		switch (type) {
			case BLUE_TYPE:
				for(int i = 0; i < size && flag; i++) {
					if(bricks[i].getContent() == _value)
						flag = false;
				}
				break;
			case YELLOW_TYPE:
				break;
			case PURPLE_TYPE:
			int counter = 0;
				for(int i = 0; i < size && flag; i++) {
					if(bricks[i].getContent() == _value) {
						counter++;
					}
				}
				if(counter == 0) {
					if(countEmptyBricks()-1<1) {
						flag = false;
					}
				}
				break;
		}
	}
	return flag;
}

int Storey::countEmptyBricks() {
	int counter = 0;
	for(int i = 0; i < size; i++) {
		if(!bricks[i].isSet())
			counter++;
	}
	return counter;
}

int Storey::countNonReperitiveBricks() {
	int counter = 0;
	for(int i = 0; i < size; i++) {
		bool flag = false;
		int currentValue = bricks[i].getContent();
		if(currentValue != 0) {
			for(int j = i + 1; j < size; j++) {
				if(currentValue == bricks[j].getContent())
					flag = true;
			}
			if(!flag)
				counter++;
		}
	}
	return counter;
}
