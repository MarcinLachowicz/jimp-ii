#include <SDL/SDL.h>
#include <iostream>
#include "vscreen.h"
#include "bdata.h"
#include "light.h"
#include "defines.h"

using namespace std;

#define MIN_BLINK_TIME 5
#define MAX_BLINK_TIME 500

extern vscreen *vscr;
Uint8 mode[] = {120, 130, 140, 150};


BDATA light::me("decdata/light.dat");

light::light(int _x, int _y){
	x = _x;
	y = _y;
	frame = 0;
	type = LIGHT;
	
	current_frame = me.get_surface(frame);
	
	//Draw static elements
	SDL_Rect smap;
	smap.x = (x+get_xx())/2-38;
	smap.y = y-19;
	vscr->fill_with_statics(me.get_surface(frame+1),&smap);
	
}

void light::blink(){
	if(rand() % 100 < 80) return;
	
	int blink_type = rand() % 4;
	SDL_SetAlpha(current_frame,SDL_SRCALPHA,mode[blink_type]);
}

void light::draw(SDL_Surface* dest){
	if(!is_on_the_screen()) return;
	
	SDL_Rect map;

	map.x = x-vscr->get_x();
	map.y = y-vscr->get_y();
	
	blink();
	SDL_BlitSurface(current_frame,NULL,dest,&map);
	
}
