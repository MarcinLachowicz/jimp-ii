#include <SDL/SDL.h>
#include "object.h"
#include "bdata.h"
#include "vscreen.h"

extern vscreen* vscr;

bool object::is_on_the_screen(){
	if(vscr->get_x() >= get_xx()) return false;
	if(vscr->get_xx() <= x) return false;
	if(vscr->get_y() >= get_yy()) return false;
	if(vscr->get_yy() <= y) return false;
	
	return true;
}
	

void object::draw(SDL_Surface* dest){
	if(!is_on_the_screen()) return;

	SDL_Rect dest_map;
	
	dest_map.x = x-vscr->get_x();
	dest_map.y = y-vscr->get_y();
	
	SDL_BlitSurface(current_frame,NULL,dest,&dest_map);
}
