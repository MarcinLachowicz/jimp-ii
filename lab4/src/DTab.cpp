/*
 * DTab.cpp
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#include "DTab.h"

DTab::DTab() : length(0), last(-1), tab(NULL) {
	resize(10);
}

DTab::DTab(int initLength) : length(0), last(-1), tab(NULL) {
	resize(initLength);
}

DTab::~DTab() {
	delete [] tab;
}

void DTab::resize(int newSize) {
	double *buffer = new double[newSize];

	for(int i=0; i<newSize && i <= last; i++) {
		*(buffer+i) = *(tab+i);
	}

	length = newSize;

	delete [] tab;

	tab = buffer;
}


void DTab::add(double element) {
	if(last+1 == length) {
		resize(++length);
	}

	tab[++last] = element;
}

double DTab::get(int index) {
	double result;

	if(index <= last) result = tab[index];

	return result;
}

void DTab::set(double element, int index) {
	if(index<=last) {
		tab[index] = element;
		if(index>last) last = index;
	}
}

void DTab::print() {
	for(int i=0; i<=last; i++) {
		std::cout << tab[i] <<", ";
	}
}
