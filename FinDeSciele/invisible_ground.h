//classa dla niewidzialnego gruntu, ale wciaz bariera
#include <SDL/SDL.h>
#include "object.h"

#ifndef INVISIBLE_GROUND_H
#define INVISIBLE_GROUND_H
class invisible_ground:public object{
	public:
		invisible_ground(int x, int y, int xx, int yy);
		void draw(SDL_Surface* dest){return;}
		
		int get_xx(){return xx;}
		int get_yy(){return yy;}
	protected:
		int xx, yy;
};
#endif
