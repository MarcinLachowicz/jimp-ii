/*
 * Node.h
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */

#ifndef NODE_H_
#define NODE_H_

#include <string>

class Node {
public:
	Node();
	Node(std::string);
	Node(std::string, Node*);
	virtual ~Node();

	void setData(std::string);
	void setNext(Node*);

	std::string getData();
	Node* getNext();

private:
	std::string data;
	Node* next;
};

#endif /* NODE_H_ */
