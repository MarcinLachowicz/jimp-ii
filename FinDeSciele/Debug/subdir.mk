################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../bdata.cpp \
../billy.cpp \
../colisions.cpp \
../colisives.cpp \
../corpse.cpp \
../cursor.cpp \
../decoration.cpp \
../dog.cpp \
../dogieman.cpp \
../dynamic_ground.cpp \
../game.cpp \
../heroe.cpp \
../house.cpp \
../invisible_ground.cpp \
../knife.cpp \
../knife_pocket.cpp \
../life.cpp \
../light.cpp \
../load_unload.cpp \
../menus_gfx.cpp \
../object.cpp \
../policeman.cpp \
../visible_ground.cpp \
../vscreen.cpp \
../water.cpp 

OBJS += \
./bdata.o \
./billy.o \
./colisions.o \
./colisives.o \
./corpse.o \
./cursor.o \
./decoration.o \
./dog.o \
./dogieman.o \
./dynamic_ground.o \
./game.o \
./heroe.o \
./house.o \
./invisible_ground.o \
./knife.o \
./knife_pocket.o \
./life.o \
./light.o \
./load_unload.o \
./menus_gfx.o \
./object.o \
./policeman.o \
./visible_ground.o \
./vscreen.o \
./water.o 

CPP_DEPS += \
./bdata.d \
./billy.d \
./colisions.d \
./colisives.d \
./corpse.d \
./cursor.d \
./decoration.d \
./dog.d \
./dogieman.d \
./dynamic_ground.d \
./game.d \
./heroe.d \
./house.d \
./invisible_ground.d \
./knife.d \
./knife_pocket.d \
./life.d \
./light.d \
./load_unload.d \
./menus_gfx.d \
./object.d \
./policeman.d \
./visible_ground.d \
./vscreen.d \
./water.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -lm -lSDL_mixer -lSDL_ttf `sdl-config --libs` -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


