#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <list>
#include "defines.h"
#include "load_unload.h"
#include "menus_gfx.h"

using namespace std;

Uint8 *key;
list<object*> colision_makers;
list<dynamic*> dynamics;	
list<colisives*> colisioning;

vscreen* vscr;
heroe* player;
int fullscreen_mode;


void scroll(){
	if((vscr->get_yy()-player->get_yy()) < VIRTUAL_BORDER && !vscr->has_reached_bottom_border()){
		if(!player->get_v_vct()) vscr->move(0,1);
		else vscr->move(0,player->get_v_vct());
	}
	else if((player->get_y() - vscr->get_y()) < VIRTUAL_BORDER && !vscr->has_reached_top_border()){
		if(!player->get_v_vct()) vscr->move(0,-1);
		else vscr->move(0,player->get_v_vct());
	}
	if((vscr->get_xx() - player->get_xx()) < VIRTUAL_BORDER && !vscr->has_reached_right_border()){
		if(!player->get_h_vct()) vscr->move(1,0);
		else vscr->move(player->get_h_vct(),0);
	}
	else if((player->get_x() - vscr->get_x()) < VIRTUAL_BORDER && !vscr->has_reached_left_border()){
		if(!player->get_h_vct()) vscr->move(-1,0);
		else vscr->move(player->get_h_vct(),0);
	}
	
	
}

void final_animation(SDL_Surface* screen){
	int time = 0;
	int game_loop = 0;
	bool go = true;
	
	Mix_Music* music = Mix_LoadMUS("gdata/eog.ogg");
	Mix_PlayMusic(music,-1);
	
	while(time != -1){
		key[SDLK_UP] = 0;
		key[SDLK_DOWN] = 0;
		key[SDLK_LEFT] = 0;
		key[SDLK_RIGHT] = go;
		key[SDLK_SPACE] = 0;
		game_loop = SDL_GetTicks();
		
		vscr->draw(screen);
			
		list<dynamic*>::iterator it;
		for(it = dynamics.begin();it != dynamics.end();it++)
			(*it)->draw(screen);
		
		for(it = dynamics.begin(); it != dynamics.end();){
			if(!(*it)->live()){
				delete (*it);
				colisioning.remove((colisives*)(*it));
				colision_makers.remove(*it);
				it = dynamics.erase(it);
				continue;
			}
			it++;
		}	
		
		player->correct();
		
		list<colisives*>::const_iterator iit;
		for(iit = colisioning.begin(); iit != colisioning.end();iit++)
			(*iit)->find_colisions();
			
		
		SDL_PumpEvents();	
		time += LOOP_DELAY;
		if(time >= 2700){
			 go = false;
			 if(time >= 9000) time = -1;
		}
		if(key[SDLK_ESCAPE]) time = -1;
		
		scroll();	
		SDL_Flip(screen);
		
		//spowolnienie gry
		int temp_delay = SDL_GetTicks() - game_loop;
		if(temp_delay < LOOP_DELAY) SDL_Delay(LOOP_DELAY-temp_delay);	
		
	}
	
	fade_out(screen,50,50);
	Mix_FreeMusic(music);
	

}

void behind_bars(SDL_Surface* screen){
	TTF_Font* bar_font = TTF_OpenFont("gdata/betad.ttf",50);
	SDL_Color looser_color= {240,12,30};
	SDL_Surface* text_surface = TTF_RenderText_Solid(bar_font,"LOOSER...",looser_color);
	SDL_Rect looser_map;
	
	looser_map.x = (MAX_X-text_surface->w)/2;
	looser_map.y = (MAX_Y-text_surface->h)/2;
	for(Uint8 alpha = 0; alpha <= 100; alpha++){
		SDL_SetAlpha(text_surface,SDL_SRCALPHA,alpha);
		SDL_BlitSurface(text_surface,NULL,screen,&looser_map);
		SDL_Flip(screen);
		SDL_Delay(50);
	}
	SDL_FreeSurface(text_surface);
	TTF_CloseFont(bar_font);
}

void winner(SDL_Surface* screen){
	TTF_Font* winner_font = TTF_OpenFont("gdata/betad.ttf",50);
	SDL_Color winner_color= {12,255,30};
	SDL_Surface* text_surface = TTF_RenderText_Solid(winner_font,"WINNER!!!",winner_color);
	SDL_Rect winner_map;
	
	winner_map.x = (MAX_X-text_surface->w)/2;
	winner_map.y = (MAX_Y-text_surface->h)/2;
	for(Uint8 alpha = 0; alpha <= 50; alpha++){
		SDL_SetAlpha(text_surface,SDL_SRCALPHA,alpha);
		SDL_BlitSurface(text_surface,NULL,screen,&winner_map);
		SDL_Flip(screen);
		SDL_Delay(50);
	}
	SDL_FreeSurface(text_surface);
	TTF_CloseFont(winner_font);
	
	final_animation(screen); 
}

void game(SDL_Surface* screen){
	int status = 0;
	Uint32 game_loop = 0;
	
	load("gdata/level0.dat");
	Mix_Music* music = Mix_LoadMUS("gdata/music_game.ogg");
	
	Mix_PlayMusic(music,-1);
	SDL_Event event;
	SDL_PollEvent(&event);
		
	while(event.type != SDL_QUIT && !status){
		game_loop = SDL_GetTicks();
		
		status = player->get_status();
		if(key[SDLK_ESCAPE]) status = -3;
		
		vscr->draw(screen);
			
		list<dynamic*>::iterator it;
		for(it = dynamics.begin();it != dynamics.end();it++)
			(*it)->draw(screen);;
		
		SDL_PumpEvents();
		
		for(it = dynamics.begin(); it != dynamics.end();){
			if(!(*it)->live()){
				delete (*it);
				colisioning.remove((colisives*)(*it));
				colision_makers.remove(*it);
				it = dynamics.erase(it);
				continue;
			}
			it++;
		}
		
		player->correct();
			
		list<colisives*>::const_iterator iit;
		for(iit = colisioning.begin(); iit != colisioning.end();iit++)
			(*iit)->find_colisions();
		
		scroll();	
		SDL_Flip(screen);
		SDL_PollEvent(&event);
		
		//spowolnienie gry
		int temp_delay = SDL_GetTicks() - game_loop;
		if(temp_delay < LOOP_DELAY) SDL_Delay(LOOP_DELAY-temp_delay);
		

	}	

	fade_out(screen,20,30);	
	
	if(status == -1) behind_bars(screen);
	else if(status == 1){
		winner(screen);
		scores(screen,player->get_scores());
	}
	
	unload();
	Mix_FreeMusic(music);
}

int main(){
	srand(time(NULL));
	int choose,intr;
	Uint32 flags = SDL_HWSURFACE | SDL_DOUBLEBUF;
	
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) cout << "SDL Error";
	if(Mix_OpenAudio(44100,AUDIO_S16SYS,2,2048) < 0) cout << "Audio Error";
	if(TTF_Init() <0) cout << "Font error";
	SDL_WM_SetCaption("Fin de siecle","FDS");
	
	load_config(intr,fullscreen_mode);
	if(fullscreen_mode) flags |= SDL_FULLSCREEN;
	SDL_Surface* screen = SDL_SetVideoMode(640,480,32,flags);
	Mix_Music* menu_music = Mix_LoadMUS("gdata/menu_music.ogg");
	key = SDL_GetKeyState(NULL);
	
	if(intr) intro(screen);
	
      	do{
      		if(!Mix_PlayingMusic()) Mix_PlayMusic(menu_music,-1);
		choose = menu(screen);
		switch(choose){
			case 0: fade_out(screen,50);
				game(screen); 
				break;
			case 1: options(screen); break;
			case 2: scores(screen,9999999); break;
			case 3: fade_out(screen,50);
				intro(screen); 
				break;
			case 4: write_config(intr,fullscreen_mode);
				fade_out(screen,50);
				madeit(screen);
				break;
		}
	}while(choose != 4);	
	Mix_FreeMusic(menu_music);
	
	SDL_Quit();
	TTF_Quit();

return 0;
}
