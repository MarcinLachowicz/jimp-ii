//klasa dla obiekt�w bed�cych dekoracjami statycznymi.
//obiekty tej klasy beda "wmalowywane" na wirtualny ekran i
//niszczone.
#include <SDL/SDL.h>
#include "object.h"
#include "bdata.h"

#ifndef DECORATION_H
#define DECORATION_H
class decoration: public object{
	public:
		decoration(int,int,int);
		virtual ~decoration(){}
		
		void draw(SDL_Surface* dest);
		void give_local_barierre(int);
	private:
		static BDATA me;
};
#endif 
