//klassa bedaca type pa�ki policyjnej.
#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef BILLY_H
#define BILLY_H
class billy:public colisives{
	public:
		billy(int,int,int);
		virtual ~billy();
		
		bool live();
		void find_colisions();
		void draw(SDL_Surface* dest){object::draw(dest);}
		void animate();
		
	protected:
		int hvct;
		bool hit_something;
		int distance,
		    direction;
		int time;
	private:
		static BDATA me;
};
#endif 
