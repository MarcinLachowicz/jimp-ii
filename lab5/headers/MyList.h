/*
 * MyList.h
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */

#ifndef MYLIST_H_
#define MYLIST_H_

#include <iostream>

#include "Node.h"

using namespace std;

class MyList {
public:
	MyList();
	MyList(const MyList&);
	virtual ~MyList();

	int getLength() const;

	void pushFront(string);
	void pushBack(string);

	void popFront();
	void popBack();

	void setNode(string, int);
	Node* getNode(int) const;

	void print();

private:
	Node* head;
};

#endif /* MYLIST_H_ */
