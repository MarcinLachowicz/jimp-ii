#include <iostream>
#include <fstream>
#include <list>
#include "load_unload.h"
#include "object.h"
#include "bdata.h"
#include "decoration.h"
#include "dogieman.h"
#include "heroe.h"
#include "vscreen.h"
#include "dog.h"
#include "policeman.h"
#include "runner.h"
#include "visible_ground.h"
#include "invisible_ground.h"
#include "dynamic_ground.h"
#include "knife_pocket.h"
#include "light.h"
#include "water.h"
#include "life.h"

using namespace std;

extern list<object*> colision_makers;
extern list<dynamic*> dynamics;	
extern list<colisives*> colisioning;
extern vscreen* vscr;
extern heroe* player;



vscreen* read_vscreen(ifstream& input){
	int level;
	input >> level;
	return new vscreen(level);
}

policeman* read_policeman(ifstream& input){
	int x, y;
	input >>x >> y;
	return new policeman(x,y);
}

runner* read_runner(ifstream& input) {
	int x, y;
	input >> x >> y;
	return new runner(x, y);
}

dogieman* read_dogieman(ifstream& input){
	int x, y;
	input >> x >> y;
	return new dogieman(x,y);
}

heroe* read_heroe(ifstream& input){
	int x, y;
	input >> x >> y; 
	return new heroe(x,y);
}

visible_ground* read_visible_ground(ifstream& input){
	char df_name[20];
	int x, y,xw,xy,pattern;
	input >> x >> y >> xw >> xy >> pattern;
	visible_ground* nvg = new visible_ground(x,y,xw,xy,pattern);
	vscr->fill_with_statics(nvg);
	return nvg;
}

invisible_ground* read_invisible_ground(ifstream& input){
	int x, y, xx, yy;
	input >> x >> y >> xx >> yy;
	return new invisible_ground(x,y,xx,yy);
}

decoration* read_decoration(ifstream& input){
	int x,y,pattern;
	input >> x >> y >> pattern;
	decoration nd(x,y,pattern);
	vscr->fill_with_statics(&nd);

	return NULL;
}

light* read_light(ifstream& input){
	int x,y;
	input >> x>> y;
	return new light(x,y);
}

water* read_water(ifstream& input){
	int x,y;
	input >> x >> y;
	return new water(x,y);
}

house* read_house(ifstream& input){
	int x,y;
	input >> x >> y;
	return new house(x,y);
}
dynamic_ground* read_dynamic_ground(ifstream& input){
	int x, y, x_amp, y_amp, pattern;
	input >> x >> y >> pattern >> x_amp >> y_amp;
	return new dynamic_ground(x,y,pattern,x_amp,y_amp);
}
knife_pocket* read_knife_pocket(ifstream& input){
	int x, y, ammo;
	input >> x >> y >> ammo;
	return new knife_pocket(x,y,ammo);
}
dog* read_dog(ifstream& input){
	int x, y;
	input >> x >> y;
	return new dog(x,y);
}
life* read_life(ifstream& input){
	int x, y;
	input >> x >> y;
	return new life(x,y);
}

bool load(const char* filename){
	ifstream file(filename,ios::in);
	string str;
	object* new_colision_maker;
	dynamic* new_dynamic;
	colisives* new_colisioning;
	
	if(!file) return false;    //komunikaty odnosnie zlego pliku
	
	file >> str;
	if(str == "vscreen" && (vscr = read_vscreen(file))); //pierwszy zawsze powinien byc umieszczony vscreen
	else return false;
	
	while(file >> str){
		new_colision_maker = new_dynamic= new_colisioning = 0;
		
		if(str == "heroe" && (new_colision_maker = new_dynamic = new_colisioning = player =read_heroe(file)));
		else if(str == "visible_ground" && (new_colision_maker= read_visible_ground(file)));
		else if(str == "invisible_ground" && (new_colision_maker = read_invisible_ground(file)));
		else if(str == "dynamic_ground" && (new_colision_maker = new_dynamic = read_dynamic_ground(file)));
		else if(str == "policeman" && (new_colision_maker = new_dynamic = new_colisioning = read_policeman(file)));
		else if(str == "runner" && (new_colision_maker = new_dynamic = new_colisioning = read_runner(file)));
		else if(str == "dogieman" && (new_colision_maker = new_dynamic = new_colisioning = read_dogieman(file)));
		else if(str == "light" && (new_colision_maker = new_dynamic = read_light(file)));
		else if(str == "decoration") read_decoration(file);
		else if(str == "water" && (new_colision_maker = new_dynamic = read_water(file)));
		else if(str == "knife_pocket" && (new_dynamic = new_colisioning = read_knife_pocket(file)));
		else if(str == "house" && (new_colision_maker = new_dynamic = new_colisioning = read_house(file)));
		else if(str == "dog" && (new_colision_maker = new_dynamic = new_colisioning = read_dog(file)));
		else if(str == "life" && (new_dynamic = new_colisioning = read_life(file)));
		else return false;
		
		if(new_dynamic) dynamics.push_back(new_dynamic);
		if(new_colisioning) colisioning.push_back(new_colisioning);
		if(new_colision_maker) colision_makers.push_back(new_colision_maker);
	}
	
	if(!player) return false; //heroe musi zostac wczytany
	return true;	
}

void load_config(int & intr,int& fscr){
	fstream config("gdata/finde.conf",ios::in | ios::out);
	if(!config) cout << "Error while reading config\n";
	
	config >> intr >> fscr;	
	if(!intr) config << "0\n" << fscr;

	config.close();
} 

void write_config(int intr, int fscr){
	fstream config("gdata/finde.conf",ios::out | ios::trunc);
	if(!config) cout << "Error while writing to config\n";
	
	config << intr << endl;
	config << fscr;
	config.close();
}
		
void unload(){
	list<object*>::iterator oit;
	list<dynamic*>::iterator dit;
	list<colisives*>::iterator cit;
	
	for(cit = colisioning.begin(); cit != colisioning.end();){
		delete (*cit);
		dynamics.remove(*cit);
		colision_makers.remove(*cit);
		cit = colisioning.erase(cit);	

	}
	for(dit = dynamics.begin(); dit != dynamics.end();){
		delete (*dit);
		colision_makers.remove(*dit);
		dit = dynamics.erase(dit);		
	}
	for(oit = colision_makers.begin(); oit != colision_makers.end();){
		delete (*oit);
		oit = colision_makers.erase(oit);
	}
	delete vscr;

}
