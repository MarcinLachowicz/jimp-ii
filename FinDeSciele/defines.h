//plik wa�niejszychg definicji

#ifndef DEFINES_H
#define DEFINES_H
//minimalna czcionka
#define MIN_FONT 30 
//maxymalna czcionka
#define MID_FONT 40

//definiecje zwiazane z decyzjami o podjeciu ataku
#define BLIND_SIGHT 150
#define SIGHT 300
#define HEAR_BORDER 100
#define HEIGTH_BORDER 20
#define HIT_BORDER 20
#define LOOP_DELAY 25

//definicje rozmiaru ekranu
#define MIN_X 0
#define MIN_Y 0
#define MAX_X 640
#define MAX_Y 480

//wirtualna granica dla kt�rej bedzie przwijany ekran
#define VIRTUAL_BORDER 180

#define GRAVITY 2

//definicje typow kolizyjnych
#define CORPSE 1
#define BARRIER 2
#define ENEMY 3
#define HEROE 4
#define BULLET 6
#define LIGHT 7
#define KNIFE 8
#define WATER 9
#define EOG 10
#define KNIFE_POCKET 11
#define UNKNOWN 0

//definecje rodzaju kolizji
#define TOP 1
#define BOTTOM -TOP
#define EDGE_L 3
#define EDGE_R -EDGE_L

#define RIGHT 1
#define LEFT -RIGHT
#define DOWN 1
#define UP -DOWN

#endif
