#include <SDL/SDL.h>
#include "vscreen.h"
#include "defines.h"
#include "bdata.h"
#include "water.h"

#define WATER_DELAY 5

extern vscreen* vscr;

BDATA water::me("wdata/water.dat",255,255,255);

water::water(int _x,int _y):time(0){
	x = _x;
	y = _y;
	frame = 0;
	current_frame = me.get_surface(frame);
	type = WATER;
	
	SDL_Rect erase_map = {x,y-5,current_frame->w,current_frame->h};
	vscr->erase(&erase_map);
}

bool water::live(){
	animate();
	return true;
}

void water::animate(){
	if(time++ < WATER_DELAY) return;
	time = 0;
	if(frame == 0) frame = 1;
	else if(frame == 1) frame = 2;
	else if(frame == 2) frame = 3;
	else if(frame == 3) frame = 4;
	else if(frame == 4) frame = 5;
	else if(frame == 5) frame = 6;
	else if(frame == 6) frame = 7;
	else if(frame == 7) frame = 8;
	else if(frame == 8) frame = 9;
	else if(frame == 9) frame = 10;
	else if(frame == 10) frame = 0;
	
	current_frame = me.get_surface(frame);
}
	 
