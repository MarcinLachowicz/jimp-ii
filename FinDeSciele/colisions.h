//colisions to klasa obiekt�w administrujacych kolizjami. One sa odpowiedzialne
//za rozstrzygniecie czy istnieje kolizja, z jakim obiektem i jakiego typu.
//Tutaj takze odbywa sie prymitywna korekta wsp�rz�dnych przy kolizjach z barierami
#include <SDL/SDL.h>
#include <vector>

#ifndef COLISIONS_H
#define COLISIONS_H

class object;
class colisives;
class colisions{
	public:
		colisions(colisives*);
		~colisions(){}
	
		object* has_with(int,int);
		object* has_with(int);
		bool has_any();
		int has_already(object*);
		int what_side(object*);
		  
		void set(object*,int,int);
		
		void calculate(object*);
		bool just_say(object*);
		
		void correction(object*,int);
		void reset();
	private:
		int &x,&y;
		int xx,yy;
		colisives* main_obj;
		
		struct pocket{
			pocket(object* o, int t, int s):colided(o),type(t),side(s){}
			object* colided;
			int type;
			int side;
		};
		std::vector<pocket> with_what;
};	 

#endif
