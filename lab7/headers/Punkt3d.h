/*
 * Punkt3d.h
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#ifndef PUNKT3D_H_
#define PUNKT3D_H_

#include <cmath>
#include <iostream>
#include "Punkt.h"

using namespace std;

class Punkt3d : public Punkt {
	public:
		Punkt3d();
		Punkt3d(double, double, double);
		 ~Punkt3d();
		double distance(Punkt3d);

		double getZ();
		void setZ(double);
	private:
		double z;
};

#endif /* PUNKT3D_H_ */
