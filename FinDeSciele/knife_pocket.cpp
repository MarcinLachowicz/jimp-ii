#include <SDL/SDL.h>
#include <list>
#include "defines.h"
#include "heroe.h"
#include "colisives.h"
#include "colisions.h"
#include "knife_pocket.h"
#include "bdata.h"

using namespace std;

extern heroe* player;
extern list<object*> colision_makers;

BDATA knife_pocket::me("hdata/kpdata/kp.dat");

knife_pocket::knife_pocket(int _x, int _y, int ammo)
:taken(false),knives(ammo){
	x = _x;
	y = _y;
	frame = 0;
	type = KNIFE_POCKET;
	
	current_frame = me.get_surface(frame);
	my_colisions = new colisions(this);
}

bool knife_pocket::live(){
	if(taken) player->give_ammo(knives);
	my_colisions->reset();
	return !taken;
}

void knife_pocket::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it)->is() == HEROE)
			taken = my_colisions->just_say(*it);
}
