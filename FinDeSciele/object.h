//podstawowa klasa dla wszystkich obiektów wyświetlanych w grze. 
#include <SDL/SDL.h>
#include "defines.h"

#ifndef OBJECT_H
#define OBJECT_H
class object{
	public:
		virtual ~object(){}
	
		virtual void draw(SDL_Surface*) = 0;
		virtual void move(int dx, int dy){x+=dx;y+=dy;}
		virtual int is(){return type;}
		virtual bool is_on_the_screen();
		virtual bool is_seen(){return true;}
		
		virtual int get_x(){return x;}
		virtual int get_y(){return y;}
		virtual int get_xx(){return (x+current_frame->w);}
		virtual int get_yy(){return (y+current_frame->h);}
	protected:
		int x,y,type;     		// atributes 
		SDL_Surface* current_frame;	// current frame from "me"
};

#endif

