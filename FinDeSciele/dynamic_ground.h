//bariery(grunt) kt�ry moze sie poruszac - nie jest satycznie "wmalowywany" w vscreen
#include <SDL/SDL.h>
#include "bdata.h"
#include "dynamic.h"

#ifndef DYNAMIC_GROUND
#define DYNAMIC_GROUND
class dynamic_ground: public dynamic{
	public:
		dynamic_ground(int,int,int,int,int);
		virtual ~dynamic_ground(){}
		
		virtual void draw(SDL_Surface* dest){object::draw(dest);}
		bool live();
	private:
		static BDATA me;
		int x_amplitude,
		    y_amplitude,
		    x_direction,
		    y_direction;
};
#endif
