#include <SDL/SDL.h>
#include <list>
#include "bdata.h"
#include "defines.h"
#include "house.h"
#include "object.h"
#include "colisions.h"
#include "vscreen.h"


BDATA house::me("hdata/house/house.dat");

using namespace std;

extern list<object*> colision_makers;
extern vscreen* vscr;


house::house(int _x, int _y):heroe_is_in(false){
	frame = 0;
	x = _x;
	y = _y;
	type = EOG;
	
	SDL_Rect where = {x+30,y-218};
	vscr->fill_with_statics(me.get_surface(3),&where);
	my_colisions = new colisions(this);
	current_frame = me.get_surface(frame);
}
house::~house(){
	delete my_colisions;
}

void house::draw(SDL_Surface* dest){
	if(!is_on_the_screen()) return;
	SDL_Rect where_gate = {x-vscr->get_x(),y};
	SDL_Rect where_fence = {x-vscr->get_x()+400,y+166};
	
	SDL_BlitSurface(current_frame,NULL,dest,&where_gate);
	SDL_BlitSurface(me.get_surface(2),NULL,dest,&where_fence);
}

void house::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it)->is() == HEROE ) heroe_is_in = my_colisions->just_say(*it);
}
bool house::live(){
	if(heroe_is_in){
		frame = 1;
		current_frame = me.get_surface(frame);
	}else if(frame == 1 && !heroe_is_in) type = BARRIER;
	
	return true;
}

