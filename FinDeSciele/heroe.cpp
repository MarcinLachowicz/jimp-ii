//heroe.cpp
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <list>
#include "defines.h"
#include "heroe.h"
#include "colisions.h"
#include "vscreen.h"
#include "bdata.h"
#include "knife.h"
#include "knife_pocket.h"

#include <iostream>

#define HEROE_DELAY 15
#define HEROE_JUMP 5
#define HEROE_POWER 5

using namespace std;

BDATA heroe::me("hdata/heroe.dat");

extern Uint8* key;
extern vscreen* vscr;
extern list<object*> colision_makers;
extern list<dynamic*> dynamics;
extern list<colisives*> colisioning;



heroe::heroe(int _x,int _y)
:vvct(0),hvct(0),direction(RIGHT),reloading(0),knives(10),lifes(3),godmode(0),scores(0),
is_in_the_light(false),status(0){
	frame = 0;
	x = _x;
	y = _y;
	type = HEROE;
	current_frame = me.get_surface(frame);
	my_colisions = new colisions(this);
}

heroe::~heroe(){
    delete my_colisions;
}

void heroe::speed_control(){
	if(vvct >= HEROE_JUMP*3) vvct = HEROE_JUMP*3;
	if(hvct > HEROE_POWER) hvct = HEROE_POWER;
	else if(hvct < -HEROE_POWER) hvct = -HEROE_POWER;
}

void heroe::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it != this ) && (*it)->is_on_the_screen())
			my_colisions->calculate(*it);
}


void heroe::animate(){
	static int time = 0;
		
	if(vvct){
		if(direction < 0) frame = 0;
		else frame = 2;
		current_frame = me.get_surface(frame);
		return;
	}
	if(hvct == 0){
		if(godmode) SDL_SetAlpha(current_frame,SDL_SRCALPHA,255);
		
		if(direction < 0) frame = 0;
		else frame = 2;
		current_frame = me.get_surface(frame);
		return;
	}	
	
	if(time++ < HEROE_DELAY) return;
	if(hvct < 0){
		time = 0;
		if(frame != 0) frame = 0;
		else frame = 1;
	}
	else{
		time = 0;
		if(frame != 2) frame = 2;
		else frame = 3;
	}
	
	
	current_frame = me.get_surface(frame);
}

void heroe::draw(SDL_Surface* dest){
	if(godmode){
		godmode--;
		if(godmode % 20 > 10) SDL_SetAlpha(current_frame,SDL_SRCALPHA,150);
		else if(godmode % 10) SDL_SetAlpha(current_frame,SDL_SRCALPHA,255);
		if(godmode == 0) SDL_SetAlpha(current_frame,SDL_SRCALPHA,255);
	}else SDL_SetAlpha(current_frame,SDL_SRCALPHA,255);
	
	SDL_Rect dest_map;
	
	
	dest_map.x = x-vscr->get_x();
	dest_map.y = y-vscr->get_y();

	SDL_BlitSurface(current_frame,NULL,dest,&dest_map);
	Panel.draw(dest,lifes,knives,scores);
}

void heroe::throw_knife(){
	if(!knives || reloading) return;
	reloading = 20; 
	knives--;
	
	//making bullet
	knife* nk = new knife(x-3+55*direction,y+15,direction);
	colision_makers.push_back(nk);
	dynamics.push_front(nk);
	colisioning.push_back(nk);
}

void heroe::correct(){
	find_colisions();
	if(!(my_colisions->has_with(BARRIER,TOP)) || vvct < 0){
		if(my_colisions->has_with(BARRIER,BOTTOM)) vvct=0;
		if(my_colisions->has_with(BARRIER,EDGE_L) && direction == RIGHT) hvct = 0;
		else if(my_colisions->has_with(BARRIER,EDGE_R) && direction == LEFT) hvct=0;
		vvct += GRAVITY;
	}else vvct = 0;
	if(my_colisions->has_with(WATER,BOTTOM)) status = -1;
	else if(my_colisions->has_with(WATER,TOP)) vvct /= 2;
	speed_control();
	move(hvct,vvct);
	my_colisions->reset();
}

bool heroe::live(){
        static bool free = false;
	static int time = 0;
	
	if(my_colisions->has_with(EOG)) status = 1;  //KONIEC GRY -> WYGRANA
	
	if(reloading) reloading--;
	
	if(my_colisions->has_with(LIGHT,BOTTOM)) is_in_the_light = true;
	else is_in_the_light = false;
	
	if(!godmode && (my_colisions->has_with(ENEMY) || my_colisions->has_with(BULLET))){
		if(--lifes > 0) godmode = 200;
		else status = -1;
		scores+=1000;
	}
	
	if(key[SDLK_SPACE]) throw_knife();
	    
	if(key[SDLK_LEFT]){ hvct--; direction = LEFT;}
	else if(key[SDLK_RIGHT]) {hvct++; direction = RIGHT;}
	else if(my_colisions->has_with(BARRIER,TOP)) hvct = 0; 
	if(free && key[SDLK_UP] ){
		if(my_colisions->has_with(BARRIER,TOP))
			vvct -= HEROE_JUMP;
		if(++time <= 5) vvct-=HEROE_JUMP;
		else free = false;

	}
	else if(!key[SDLK_UP] && my_colisions->has_with(BARRIER,TOP)){free = true; time = 0;}
	else if(!key[SDLK_UP]) free = false;
	
	animate();
	my_colisions->reset();

return true;
}


void heroe::panel::draw(SDL_Surface* dest,int lifes, int knives, int scores){
	SDL_Rect hmap;
	SDL_Rect mmap;
	SDL_Rect kmap;
	SDL_Color font_color= {255,185,49};
	char numbers[10] = "";
	
	hmap.x = MAX_X - 110;
	hmap.y = 20;
	mmap.x = MAX_X/2 - 70;
	mmap.y = kmap.y = hmap.y;
	kmap.x = 10;
	
	SDL_SetAlpha(me.get_surface(4),SDL_SRCALPHA,255);
	for(int l = 0; l < 3; l++){
		if(l == lifes) SDL_SetAlpha(me.get_surface(4),SDL_SRCALPHA,150);
		SDL_BlitSurface(me.get_surface(4),NULL,dest,&hmap);
		hmap.x += 34;
	}
	SDL_BlitSurface(me.get_surface(5),NULL,dest,&mmap); //obok wypisz ile
	SDL_BlitSurface(me.get_surface(6),NULL,dest,&kmap); //obok ypisz ile
	
	sprintf(numbers,"x%d",scores);
	SDL_Surface* font_surface = TTF_RenderText_Solid(my_font,numbers,font_color);
	mmap.x+=80;
	SDL_BlitSurface(font_surface,NULL,dest,&mmap);
	SDL_FreeSurface(font_surface);
	
	sprintf(numbers,"x%d",knives);
	font_surface = TTF_RenderText_Solid(my_font,numbers,font_color);
	kmap.x+=70; 
	SDL_BlitSurface(font_surface,NULL,dest,&kmap);
	SDL_FreeSurface(font_surface);
	
	
}

