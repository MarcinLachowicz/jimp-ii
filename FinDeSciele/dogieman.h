//klasa dla policjantów z psami. Policjant spuszcza psa
//tylko wtedy gdy zauwazy bohatera. Pies przy policjancie nie zaatakuje bez rozkazu.
#include <SDL/SDL.h> 
#include "bdata.h"
#include "policeman.h"
#include "dog.h"

#ifndef DOGIEMAN_H
#define DOGIEMAN_H

class dogieman: public policeman{
	public:
		dogieman(int,int);
		virtual ~dogieman();
		
		virtual void take_action();
		virtual void animate();
		virtual bool live();
		void my_dog_died(){my_dog = 0; did_my_job = true;}
		int get_dir(){return direction;}
	protected:
		dog* my_dog;
	private:
		static BDATA me;
};
#endif
