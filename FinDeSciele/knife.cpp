//knife.cpp
#include <SDL/SDL.h>
#include <list>
#include <iostream>
#include "knife.h"
#include "vscreen.h"
#include "colisions.h"
#include "defines.h"

#define KNIFE_POWER 10
#define KNIFE_RANGE 960

using namespace std;

BDATA knife::me("hdata/knife/knife.dat");
extern list<object*> colision_makers;

knife::knife(int _x,int _y,int dir)
:hit_something(false),distance(0),direction(dir){
	if(direction == LEFT) frame = 0;
	else frame = 1;
	x = _x;
	y = _y;
	type = KNIFE;
	
        hvct = direction*KNIFE_POWER;
	my_colisions = new colisions(this);
	current_frame = me.get_surface(frame);
}
knife::~knife(){
	delete my_colisions;
}

void knife::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end() && !hit_something;it++)
		if((*it)->is() == ENEMY || (*it)->is() == BARRIER)
			hit_something = my_colisions->just_say(*it);
}
bool knife::live(){
	if(hit_something) return false;
	if(distance > KNIFE_RANGE) return false;
	
	distance += KNIFE_POWER;
	move(hvct,0);
	return true;
}
 
