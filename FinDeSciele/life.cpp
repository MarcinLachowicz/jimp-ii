#include <SDL/SDL.h>
#include <list>
#include "heroe.h"
#include "defines.h"
#include "life.h"
#include "colisions.h"


#define LIFE_DELAY 10

using namespace std;

extern list<object*> colision_makers;
extern heroe* player;

BDATA life::me("hdata/life/life.dat");

life::life(int _x, int _y):taken(false){
	x = _x;
	y = _y;
	
	frame = 0;
	current_frame = me.get_surface(frame);
	my_colisions = new colisions(this);
}

life::~life(){
	delete my_colisions;
}

void life::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it)->is() == HEROE)
			taken = my_colisions->just_say(*it);
}

void life::animate(){
	if(time++ < LIFE_DELAY) return;
	time = 0;
	
	if(frame == 0) frame = 1;
	else frame = 0;
	
	current_frame = me.get_surface(frame);
}

bool life::live(){
	if(taken){
		player->give_life();
		return false;
	}
	
	animate();
	return true;
}
	
	


	
