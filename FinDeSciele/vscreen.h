//virtualny ekran - czzesc calej planszy ktora jest przekazywana do wyswietlenia na ekran
#include <SDL/SDL.h>
#include "bdata.h"
#include "defines.h"
#include "object.h"

#ifndef VSCREEN_H
#define VSCREEN_H
class object;
class vscreen: public object{
	public:
		vscreen(int);
		virtual ~vscreen(){me.reload("vscrdata/vscr.dat");}
	
		void set_level(int level);
		virtual void draw(SDL_Surface*);
		virtual bool is_on_the_screen(){return true;} 
		virtual void move(int,int);
		void fill_with_statics(object*);
		void fill_with_statics(SDL_Surface*,SDL_Rect*);
		void erase(SDL_Rect*);
		
		
		virtual int get_xx(){return x+MAX_X;}
		virtual int get_yy(){return y+MAX_Y;}
		int has_reached_left_border(){return (x <= MIN_X);}
		int has_reached_top_border(){return (y <= MIN_Y);}
		int has_reached_right_border(){return ((x+MAX_X) >= current_frame->w);}
		int has_reached_bottom_border(){return ((y+MAX_Y) >= current_frame->h);}
	protected:
		static BDATA me;
		SDL_Surface* sky;
		
};
#endif	
