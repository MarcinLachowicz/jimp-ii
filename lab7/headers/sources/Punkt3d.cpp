/*
 * Punkt3d.cpp
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#include "../Punkt3d.h"

Punkt3d::Punkt3d() :
		Punkt(0, 0) {
	cout << "konstruktor 3D" << endl;
	z = 0;

}

Punkt3d::Punkt3d(double _x, double _y, double _z) :
		Punkt(_x, _y) {
	cout << "konstruktor 3D" << endl;
	z = _z;
}
Punkt3d::~Punkt3d() {
	cout << "destruktor 3D" << endl;
	// TODO Auto-generated destructor stub
}

double Punkt3d::distance(Punkt3d inny) {
	return sqrt(pow(x - inny.getX(), 2) + pow(y - inny.getY(), 2) + pow(z - inny.getZ(), 2));
}

double Punkt3d::getZ() {
	return z;
}

void Punkt3d::setZ(double _v) {
	z = _v;
}
