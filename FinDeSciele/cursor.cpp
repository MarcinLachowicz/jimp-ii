#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "cursor.h"

using namespace std;

cursor::cursor(SDL_Surface* scr ,SDL_Surface* bg,TTF_Font* fn,SDL_Color cl,char* tp)
:screen(scr),bground(bg),font(fn),color(cl){
	type = strdup(tp);
}

void cursor::show(SDL_Rect& where){
	SDL_Surface* text_surface = TTF_RenderText_Solid(font,"_",color);
	SDL_BlitSurface(text_surface,NULL,screen,&where);
	SDL_FreeSurface(text_surface);
	SDL_Flip(screen);
}
void cursor::hide(SDL_Rect& where){
	int w,h;
	TTF_SizeText(font,type,&w,&h);
	where.h = h;
	where.w = w;
	SDL_BlitSurface(bground,&where,screen,&where);
	SDL_Flip(screen);
}
