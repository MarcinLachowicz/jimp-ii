//funkcje dla menu, oraz innyc "efekt�w specjalnych"
#include <SDL/SDL.h>
#include <iostream>

class record{
	public:
		record(){}
		record(std::string n,int s):name(n),score(s){}
		std::string name;
		int score;
};
void fade_out(SDL_Surface* screen, Uint8 step=255, int wait=10);
void intro(SDL_Surface* screen);
std::string scan(SDL_Surface* screen,SDL_Surface *bg, SDL_Rect where);
void scores(SDL_Surface* screen,int amount);			
int menu(SDL_Surface* screen);
void options(SDL_Surface* screen);
void madeit(SDL_Surface*);
void rules(SDL_Surface*);
void ir_blink(SDL_Surface* one, SDL_Surface* two,SDL_Surface* screen,int how_much);

