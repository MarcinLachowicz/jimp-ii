//klasa majaca by� przechowywalni� wszysktich bitmap �adowanych do
//programu. Kazdy obiekt ma statyczny obiekt tego typy, przez co zmniejsza sie 
//zuzycie pamieci - obiekty tych samych typ�w wsp�dziel� bitmapy.
#include <SDL/SDL.h>

#ifndef BDATA_H
#define BDATA_H

class BDATA{
	public:
		BDATA(const char*,Uint8=255,Uint8=0,Uint8=255);
		~BDATA();
		void reload(const char*,Uint8=255,Uint8=0,Uint8=255);
		
		SDL_Surface* get_surface(int frame){return frames[frame];}
		int get_fn(){return fn;}	
	protected:
		void free();
		SDL_Surface** frames;
		int fn;
};
#endif
 
