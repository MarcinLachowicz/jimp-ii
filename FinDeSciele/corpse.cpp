#include <SDL/SDL.h>
#include <list>
#include "bdata.h"
#include "defines.h"
#include "colisions.h"
#include "corpse.h"
#include "vscreen.h"

#define FATCORPSE 1
#define SLIMCORPSE 2
#define DOGCORPSE 3

using namespace std;

BDATA corpse::me("pdata/corpses/corpses.dat");

extern list<object*> colision_makers;
extern vscreen* vscr;

corpse::corpse(int _x, int _y, int pattern)
:seen(0),vvct(0){
	frame = pattern;
	x = _x;
	y = _y;
	type=CORPSE;
	
	my_colisions = new colisions(this);
	current_frame = me.get_surface(frame);
}

void corpse::draw(SDL_Surface* dest){
	if(!is_on_the_screen()) return;

	SDL_Rect dest_map;
	
	dest_map.x = x-vscr->get_x();
	dest_map.y = int(y-vscr->get_y()+(get_yy() - y)*0.2);    //here is modification
	
	SDL_BlitSurface(current_frame,NULL,dest,&dest_map);
}

corpse::~corpse(){
	delete my_colisions;
}

void corpse::animate(){
}

void corpse::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it != this ) && (*it)->is() == BARRIER || (*it)->is() == LIGHT) 
			my_colisions->calculate(*it);	
}

bool corpse::live(){
	if(my_colisions->has_with(BARRIER,TOP)) vvct = 0;
 	else vvct += GRAVITY;
	if(my_colisions->has_with(LIGHT)) seen = true;
 
	animate();
 	move(0,vvct);
 	my_colisions->reset();
 	return true;
}
	
