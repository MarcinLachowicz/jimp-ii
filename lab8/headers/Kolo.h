/*
 * Kolo.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef KOLO_H_
#define KOLO_H_

#include "Ksztalt.h"

class Kolo: public Ksztalt {
	public:
		Kolo();
		virtual ~Kolo();
		virtual void rysuj();
};

#endif /* KOLO_H_ */
