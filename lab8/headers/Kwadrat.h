/*
 * Kwadrat.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef KWADRAT_H_
#define KWADRAT_H_

#include "Ksztalt.h"

class Kwadrat: public Ksztalt {
	public:
		Kwadrat();
		virtual ~Kwadrat();

		virtual void rysuj();
};

#endif /* KWADRAT_H_ */
