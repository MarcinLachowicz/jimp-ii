/*
 * Kwadrat.h
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#ifndef KWADRAT_H_
#define KWADRAT_H_

#include "Punkt.h"

class Kwadrat {
public:
	Kwadrat();
	virtual ~Kwadrat();

	double getPerimeter();
	double getSurface();

private:
	Punkt a, b, c, d;

};

#endif /* KWADRAT_H_ */
