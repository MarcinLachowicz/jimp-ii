/*
 * UmowaPraca.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef UMOWAPRACA_H_
#define UMOWAPRACA_H_

#include "Umowa.h"

class UmowaPraca: public Umowa {

	public:
		UmowaPraca(double pensja);
		virtual double pobierzNetto();
		virtual ~UmowaPraca();

	private:
		float czescNetto;
};

#endif /* UMOWAPRACA_H_ */
