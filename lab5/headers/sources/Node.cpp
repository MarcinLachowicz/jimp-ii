/*
 * Node.cpp
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */

#include "../Node.h"
Node::Node() {

}

Node::Node(std::string _data, Node* _next) {
	data = _data;
	next = _next;
}


Node::~Node() {
	// TODO Auto-generated destructor stub
}

void Node::setData(std::string _data) {
	data = _data;
}

void Node::setNext(Node* _next) {
	next = _next;
}

std::string Node::getData() {
	return data;
}

Node* Node::getNext() {
	return next;
}
