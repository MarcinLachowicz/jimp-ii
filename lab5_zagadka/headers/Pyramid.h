/*
 * Pyramid.h
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#ifndef PYRAMID_H_
#define PYRAMID_H_

#include <vector>
#include <iostream>
#include "Storey.h"

class Pyramid {
	public:
		Pyramid(int);
		virtual ~Pyramid();

		Storey* getStorey(int _n);
		void print();
		bool insert();
		void solve();

		int countFilledBricks(Brick*, Brick*, Brick*);
		bool isWholeFilled();

	private:
		int heigth;
		std::vector<Storey> storeys;
};

#endif /* PYRAMID_H_ */
