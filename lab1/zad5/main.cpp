#include<iostream>

using namespace std;

void fill(int array[][10]);
void print(int array[][10]);

int main() {
	int array[10][10];
	
	fill(array);
	print(array);

	return 0;
}
void fill(int array[][10]) {
	for(int i=0; i<10; i++) {
		for(int j=0; j<10; j++) {
			array[i][j] = (i+1)*(j+1);		
		}
	}
}

void print(int array[][10]) {
	for(int i=0; i<10; i++) {
		for(int j=0; j<10; j++) {
			cout << array[i][j] << " ";		
		}
		cout << endl;
	}
}
