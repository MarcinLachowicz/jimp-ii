/*
 * main.cpp
 *
 *  Created on: 08-03-2013
 *      Author: marcin
 */
#include <iostream>

#include "headers/MyList.h"
#include "headers/Punkt.h"
#include "headers/DTab.h"

using namespace std;

int main(int argc, char **argv) {
	Punkt pkt;
	Punkt pkt2(pkt);

	DTab tab1;
	DTab tab2 = tab1.wypelniona(1);
	cout << endl;
	tab2.print();



	//----------------lista
	MyList list;

	cout << endl << endl << "Dlugosc listy: " << list.getLength() << endl;
	list.pushBack("jeden");
	list.pushBack("dwa");
	list.pushFront("zero");

	cout << "Dlugosc listy: " << list.getLength() << endl;

	list.print();

	list.popBack();
	list.popFront();

	cout << "Po usuwaniu: " << endl;

	list.print();

	MyList list2(list);

	return 0;
}

