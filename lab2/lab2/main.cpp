
#include <iostream>
#include <cstdlib>

using namespace std;
/*
// zad 1
void is_prime1(int);
void is_prime2(int*);
void is_prime3(int&);

int main(void) {

	int a = 0;

	cout << a << endl;
	is_prime1(a);
	cout << a << endl;
	is_prime2(&a);
	cout << a << endl;
	is_prime3(a);
	cout << a << endl;

	return 0;
}

void is_prime1(int a) {
	a = 1;
}

void is_prime2(int* a) {
	*a = 2;
}

void is_prime3(int& a) {
	a = 3;
}*/

//zad 2
/*
int * potega(int liczba, int potega)	{
	int a = liczba;
  int* wynik = &liczba;
  for(int i = 1; i < potega; i++)
    *wynik *= a;
  return wynik;
}

int main(void){
  cout << *(potega(3,3)) << endl;
} */

//zad 3
/*
void showTable(int tab[][5], int rows, int cols);

int main(void) {

	int array[][5] = {
		{1,2,3,4,5},
		{1,2,3,4,5},
		{1,2,3,4,5},
		{1,2,3,4,5},
		{1,2,3,4,5}
	};

	showTable(array, 5, 5);

	return 0;
}

void showTable(int tab[][5], int rows, int cols) {
	for(int i=0; i<rows*cols; i++) {
		cout << tab[i/5][i%5] << ", ";
		if(i%5 == 4) cout  << endl;
	}
}*/

//zad 4
/*
int porownanie(int, int);
void sort(int* tablica, int length, int (*porownanie)(int, int));

int main(void) {

	int array[4] = {4,3,2,1};
	sort(array, 4, porownanie);
	for(int i=0; i<4; i++) {
		cout << array[i] << ", ";
	}
	return 0;
}

int porownanie(int a, int b) {
	return (a<b);
}
void sort(int* tablica, int length, int (*porownanie)(int, int)) {
	do {
		for(int i=0; i<length-1; i++) {
			if(porownanie(tablica[i], tablica[i+1])) {
				int buffer = tablica[i];
				tablica[i] = tablica[i+1];
				tablica[i+1] = buffer;
			}
		}
		length--;
	}while(length>1);
}*/

//zad 5

int cmp(const void *a, const void *b);

struct data {
	char imie[10];
	char nazwisko[10];
	short rok_urodzenia;
};

int main(void) {
	cout << "Podaj 3 komplety danych" ;
	data dane[3];

	for(int i=0; i<3; i++) {
		cout << "Imie: ";
		cin >> dane[i].imie;

		cout << "Nazwisko: ";
		cin >> dane[i].nazwisko;

		cout << "Rok urodzenia: ";
		cin >> dane[i].rok_urodzenia;
	}

	qsort(dane, 3, sizeof(data), cmp);

	for(int i=0; i<3; i++) {
		cout << "Imie: ";
		cout<<dane[i].imie;

		cout << "Nazwisko: ";
		cout<<dane[i].nazwisko;

		cout << "Rok urodzenia: ";
		cout<< dane[i].rok_urodzenia << endl;
	}
	return 0;
}


int cmp(const void *a, const void *b) {
    data arg1 = *reinterpret_cast<const data*>(a);
    data arg2 = *reinterpret_cast<const data*>(b);
    if(arg1.rok_urodzenia > arg2.rok_urodzenia) return -1;
    if(arg1.rok_urodzenia < arg2.rok_urodzenia) return 1;
    return 0;
}

