#include <SDL/SDL.h>
#include <list>
#include <math.h>
#include "defines.h"
#include "bdata.h"
#include "dog.h"
#include "heroe.h"
#include "dynamic.h"
#include "object.h"
#include "colisions.h"
#include "corpse.h"
#include "dogieman.h"

using namespace std;

#define DOG_POWER 4
#define SMELLX_BORDER 300
#define SMELLY_BORDER 200
#define MAX_DOG_POWER 6
#define DOG_VCT 20
#define DOG_DELAY 15
#define DOG_JUMP -20

BDATA dog::me("pdata/dog/dog.dat");

extern heroe *player;
extern list<object*> colision_makers;
extern list<dynamic*> dynamics;
extern list<colisives*> colisioning;

dog::dog(int _x, int _y,dogieman* _owner)
:direction(LEFT),hvct(0),vvct(0),time(0),smells(false),unleached(false),owner(_owner){
	frame = 0;
	type = ENEMY;
	x = _x;
	y = _y;
	
	if(!owner) unleached = true;
	current_frame = me.get_surface(frame);
	my_colisions = new colisions(this);
}
dog::~dog(){
	if(my_colisions) delete my_colisions;
}

void dog::animate(){
	if(time++ < DOG_DELAY) return;
	
	time = 0;
	if(hvct <= DOG_POWER && hvct >= -DOG_POWER){
		if(direction <0){
			if(frame != 1) frame = 1;
			else frame = 2;
		}else{
			if(frame != 6) frame = 6;
			else frame = 7;
		}
	}else{
		if(direction <0){
			if(frame != 3) frame = 3;
			else frame = 4;
		}else{
			if(frame != 8) frame = 8;
			else frame = 9;
		}
	}
	
	current_frame = me.get_surface(frame);
	
}
bool dog::live(){
	object* tmp;	
	int Px = (x+get_xx())/2;
	int Hx = (player->get_x()+player->get_xx())/2;
	int Py = (y+get_yy())/2;
	int Hy = (player->get_y()+player->get_yy())/2;
	
	if(owner) direction = owner->get_dir();
	
	if(my_colisions->has_with(KNIFE)){
		corpse *nc;
		if(direction < 0) nc = new corpse(x,y-10,4);
		else nc = new corpse(x,y-10,5);
		colision_makers.push_back(nc);
		colisioning.push_back(nc);
		dynamics.push_front(nc);
		player->give_score(500);
		if(owner) owner->my_dog_died();
		return false;
	}
	
	if(unleached && fabs(Px-Hx) < SMELLX_BORDER && fabs(Py-Hy) < SMELLY_BORDER){
		 if(player->get_x() >= x && direction == RIGHT) direction = RIGHT;
		 else if(player->get_xx() <= get_xx())  direction = LEFT;
		 else direction = RIGHT;
		 
		 hvct = direction*MAX_DOG_POWER;
		 smells = true;
	}else hvct = direction*DOG_POWER;
	
	if((tmp = my_colisions->has_with(BARRIER,TOP))){
		if(unleached && !smells && ((x < tmp->get_x() && direction == LEFT) ||
		  (get_xx() > tmp->get_xx() && direction == RIGHT))){
			direction = -direction;	
			hvct = -hvct;
		}else if(smells && (player->get_yy() > get_yy()) && !my_colisions->has_with(HEROE)) vvct = DOG_JUMP;
	}
	if(unleached)
	if(my_colisions->has_with(BARRIER,EDGE_L) || my_colisions->has_with(BARRIER,EDGE_R)){
		if(smells && my_colisions->has_with(BARRIER,TOP)) vvct = DOG_JUMP;
		else if(!smells){
			direction = -direction; 
			hvct  = -hvct;
		}
	}
	if(!my_colisions->has_with(BARRIER,TOP)) vvct += GRAVITY;
	else if(vvct> 0) vvct = 0;
	
	if(vvct > DOG_VCT) vvct = DOG_VCT;
	
	animate();
	move(hvct,vvct);
	my_colisions->reset();
	smells = false;
	return true;
}
void dog::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it != this ) && (*it)->is() != ENEMY)
			my_colisions->calculate(*it);
}
	
