#include <SDL/SDL.h>
#include "visible_ground.h"
#include "defines.h"
#include "bdata.h"


BDATA visible_ground::me("brdata/barrier.dat");		//static data for all grrounds

visible_ground::visible_ground(int _x, int _y, int Xx, int Xy, int pattern){
	x = _x;
	y = _y;
	current_frame = me.get_surface(pattern);
	xx = x+Xx*current_frame->w;
	yy = y+Xy*current_frame->h;
	
	type = BARRIER;
}

void visible_ground::draw(SDL_Surface* dest){
	SDL_Rect map;
	for(int h = y; h < yy; h+=current_frame->h){
		map.y = h;
		for(int w = x;w < xx;w+=current_frame->w){
			map.x = w;
			SDL_BlitSurface(current_frame,NULL,dest,&map);
		}
	}
}
