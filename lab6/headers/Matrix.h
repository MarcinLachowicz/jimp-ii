/*
 * Matrix.h
 *
 *  Created on: 14-03-2013
 *      Author: marcin
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include "Complex.h"

class Matrix {
public:
	Matrix();
	virtual ~Matrix();
private:
	Complex* z;
};

#endif /* MATRIX_H_ */
