/*
 * Kolo.cpp
 *
 *  Created on: 17-03-2013
 *      Author: marcin
 */

#include "../Kolo.h"

Kolo::Kolo(double _x, double _y, double _r) {
	x = _x;
	y = _y;
	r = _r;
}

Kolo::~Kolo() {
	// TODO Auto-generated destructor stub
}

double Kolo::pole() {
	return PI*r*r;
}

