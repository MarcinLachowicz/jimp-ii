#include <SDL/SDL.h>
#include "bdata.h"
#include <fstream>
#include <iostream>

using namespace std;

BDATA::BDATA(const char* name,Uint8 R, Uint8 G, Uint8 B):fn(0),frames(0){
	if(name){
		fstream file(name,ios::in);
		char bmp_name[50];
				
		file >> fn;
		frames = new SDL_Surface*[fn];
		for(int counter = 0; counter < fn; counter++){;
			file >> bmp_name;
			frames[counter] = SDL_LoadBMP(bmp_name);
			SDL_SetColorKey(frames[counter],SDL_SRCCOLORKEY, SDL_MapRGB(frames[counter]->format,R,G,B));
		}
	}
}

BDATA::~BDATA(){
	if(frames) free();
}

void BDATA::free(){
	for(int counter = 0; counter < fn; counter++)
		SDL_FreeSurface(frames[counter]);
	delete [] frames;
}

void BDATA::reload(const char* name,Uint8 R, Uint8 G, Uint8 B){
	if(frames) free();
	fn = 0;
	frames = 0;
	if(name){
		fstream file(name,ios::in);
		char bmp_name[50];
				
		file >> fn;
		frames = new SDL_Surface*[fn];
		for(int counter = 0; counter < fn; counter++){;
			file >> bmp_name;
			frames[counter] = SDL_LoadBMP(bmp_name);
			SDL_SetColorKey(frames[counter],SDL_SRCCOLORKEY, SDL_MapRGB(frames[counter]->format,R,G,B));
		}
	}
}
	
