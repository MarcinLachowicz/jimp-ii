/*
 * Brick.h
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#ifndef BRICK_H_
#define BRICK_H_

#include <iostream>

class Brick {
	public:
		Brick();
		virtual ~Brick();

		void presetContent(int);
		void setContent(int);
		void incrementContent();
		int getContent() const;

		void setConstancy(bool);
		bool isConstant() const;
		bool isSet() const;
	private:
		int content;
		bool constancy;
};

#endif /* BRICK_H_ */
