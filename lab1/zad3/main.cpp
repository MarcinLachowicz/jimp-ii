#include <iostream>

using namespace std;

void print(char array[], int i=0);
 
int main() {
	char array[] = "abcd";

	print(array);
	cout << endl;

	return 0;
}

void print(char array[], int i) {
	if(array[i] != '\0') print(array, i+1); 
	cout << array[i];
}
