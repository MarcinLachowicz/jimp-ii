//klasa dla tzw. "�y�"
#include <SDL/SDL.h>
#include "colisives.h"
#include "bdata.h"

#ifndef LIFE_H
#define LIFE_H
class life:public colisives{
	public:
		life(int , int);
		virtual ~life();
		
		virtual void draw(SDL_Surface* dest){object::draw(dest);}
		virtual bool live();
		virtual void find_colisions();
		void animate();
		
	private:
		static BDATA me;
		bool taken;
		int time;
};
#endif
