/*
 * Brick.cpp
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#include "../Brick.h"

Brick::Brick() {
	setContent(0);
	setConstancy(false);
}

Brick::~Brick() {
	// TODO Auto-generated destructor stub
}

void Brick::presetContent(int _content) {
	setContent(_content);
	setConstancy(true);
}

void Brick::setContent(int _content) {
	if(!constancy) {
		content = _content;
		//std::cout << "set: " << _content << std::endl;
	}
}

int Brick::getContent() const {
	return content;
}

void Brick::setConstancy(bool _constancy) {
	constancy = _constancy;
}

bool Brick::isConstant() const {
	return constancy;
}

bool Brick::isSet() const {
	return (content != 0);
}

void Brick::incrementContent() {
	content++;
}
