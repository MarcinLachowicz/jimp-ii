/*
 * Dziecko.h
 *
 *  Created on: 09-03-2013
 *      Author: marcin
 */

#ifndef DZIECKO_H_
#define DZIECKO_H_

#include <iostream>

using namespace std;

class Dziecko {
	friend class Rodzic;
public:
	Dziecko();
	virtual ~Dziecko();
private:
	string imie;
	string nazwisko;
	int wiek;
	string szkola;
};

#endif /* DZIECKO_H_ */
