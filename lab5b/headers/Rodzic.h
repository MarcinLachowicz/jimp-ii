/*
 * Rodzic.h
 *
 *  Created on: 09-03-2013
 *      Author: marcin
 */

#ifndef RODZIC_H_
#define RODZIC_H_

#include <iostream>
#include "Dziecko.h"

using namespace std;

class Rodzic {
public:
	Rodzic();
	virtual ~Rodzic();

	void przepiszDoInnejSzkoly(string);
private:
	string imie;
	string nazwisko;
	int wiek;
	Dziecko dziecko;
};

#endif /* RODZIC_H_ */
