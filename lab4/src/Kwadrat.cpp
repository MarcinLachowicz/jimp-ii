/*
 * Kwadrat.cpp
 *
 *  Created on: 02-03-2013
 *      Author: marcin
 */

#include "Kwadrat.h"

Kwadrat::Kwadrat(Punkt _a, Punkt _b, Punkt _c, Punkt _d) {
	a = _a;
	b = _b;
	c = _c;
	d = _d;
}

Kwadrat::~Kwadrat() {

}

double Kwadrat::getPerimeter() {
	return (4*a.distance(b));
}

double Kwadrat::getSurface() {
	double sideLength = a.distance(b);

	return (sideLength*sideLength);
}
