#include <SDL/SDL.h>
#include <iostream>
#include <list>
#include <math.h>
#include "policeman.h"
#include "billy.h"
#include "defines.h"
#include "heroe.h"
#include "colisions.h"
#include "dynamic.h"
#include "bdata.h"
#include "vscreen.h"
#include "corpse.h"


#define POLICE_MAX 20
#define POLICEMAN_POWER 3
#define POLICEMAN_JUMP -15
#define POLICE_DELAY 15
#define TG_ALFA 0.58


using namespace std;

BDATA policeman::me("pdata/policeman/policeman.dat");

extern heroe *player;
extern list<object*> colision_makers;
extern list<dynamic*> dynamics;	
extern list<colisives*> colisioning;
extern vscreen* vscr;


policeman::policeman(int _x, int _y,bool _did)
:hvct(0),vvct(0),direction(RIGHT),reloading(0),did_my_job(_did),time(0),action(0){
	frame = 0;
	x = _x;
	y = _y;
	type = ENEMY;
	
	current_frame = me.get_surface(frame);
	my_colisions = new colisions(this);
}

policeman::~policeman(){
	if(my_colisions) delete my_colisions;
}

 
inline double policeman::f1(int ex){
	return (TG_ALFA*ex - TG_ALFA*((x+get_xx())/2) + (y+get_yy())/2);
}

inline double policeman::f2(int ex){
	return (-TG_ALFA*ex +TG_ALFA*((x+get_xx())/2) + (y+get_yy())/2);
}

void policeman::animate(){

	if(action > 0){
		--action;
		if(direction < 0){
			if(action > 10)frame = 3;
			else frame = 4;
		}
		else{
			if(action > 10) frame = 8;
			else frame = 9;
		}
		current_frame = me.get_surface(frame);
		return;
	}
	else if(action < 0){
		action++;
		if(direction < 0) frame  = 10;
		else frame = 11;
		current_frame = me.get_surface(frame);
		return;
	}

	if(vvct){
		if(direction < 0) frame = 0;
		else frame = 5;
		current_frame = me.get_surface(frame);
		return;
	}
	if(hvct == 0){
		if(direction < 0) frame = 0;
		else frame = 5;
		current_frame = me.get_surface(frame);
		return;
	}	
	
	if(time++ < POLICE_DELAY) return;
	if(hvct < 0){
		time = 0;
		if(frame != 1) frame = 1;
		else frame = 2;
	}
	else{
		time = 0;
		if(frame != 6) frame = 6;
		else frame = 7;
	}
	
	
	current_frame = me.get_surface(frame);
}
bool policeman::live(){
	object* tmp = 0;
	
	if(my_colisions->has_with(KNIFE)){
		corpse *nc;
		if(direction < 0) nc = new corpse(x,y,2);
		else nc = new corpse(x,y,3);
		colision_makers.push_back(nc);
		colisioning.push_back(nc);
		dynamics.push_front(nc);
		player->give_score(1000);
		return false;
	}
	
	if(reloading) reloading--;
	
	hvct = direction*POLICEMAN_POWER;
	if(tmp = my_colisions->has_with(BARRIER,TOP)){
		if((x < tmp->get_x() && direction == LEFT) ||
		  (get_xx() > tmp->get_xx() && direction == RIGHT)){
			direction = -direction;	
			hvct = -hvct;
		}
	}
	if(my_colisions->has_with(BARRIER,EDGE_L) || my_colisions->has_with(BARRIER,EDGE_R)){
		direction = -direction; 
		hvct  = -hvct;
	}
	if(!my_colisions->has_with(BARRIER,TOP)) vvct += GRAVITY;
	else if(vvct >0) vvct = 0;
	
	if(found_sth() || action) hvct = 0;
	else if(fabs(get_yy() - player->get_yy()) < HEIGTH_BORDER && 
		fabs(x - player->get_x()) < HEAR_BORDER)
		direction = -direction;
	 

	if(vvct > POLICE_MAX) vvct = POLICE_MAX;
	
	animate();
	move(hvct,vvct);
	my_colisions->reset();
	
 	return true;
}

bool policeman::found_sth(){
	if(is_on_the_screen() && see_him(player) && !action){
		take_action();
		return true;
	}

	list<dynamic*>::iterator it = dynamics.begin();
	for(it;it!=dynamics.end();it++){
		if((*it)->is() == CORPSE)
			if(see_him(*it)){
			
				if(!action){
					 action = -30; 
					 return true;
				}else if(action != -1) return true;
				
				player->give_score(200);
				policeman* sir = new policeman((*it)->get_x(),(*it)->get_y()-41,true);
				colision_makers.push_back(sir);
				colisioning.push_back(sir);
				dynamics.push_front(sir);
				
				((corpse*)(*it))->see_you();
				return true;
			}
	}
	
	return false;
}
		
	

bool policeman::see_him(dynamic* obj){
	
	int Hx = (obj->get_x()+obj->get_xx())/2;
	int Hy = (obj->get_y()+obj->get_yy())/2;
	int Px = (x+get_xx())/2;
	int Py = (y+get_yy())/2;
	
	if(!obj->is_seen() && fabs(Hx-Px) > BLIND_SIGHT) return false;
	if(Px == Hx) return false;
	
	int a = (Py-Hy)/(Px-Hx);   //watch division by 0
	int b = a*Hx+Hy;
	int result;
	
	// CHECK IF HEROE IS SEEN
	if(direction > 0){
		if(Hy <= f1(Hx) && Hy >= f2(Hx) && Hx <= (Px+SIGHT)){
		//sprawdzenie czy nie ma przeszkod na grodze wzroku
		list<object*>::const_iterator it = colision_makers.begin();
		for(it;it!=colision_makers.end();it++){
			if((*it)->is() == BARRIER && (*it)->get_x() > Px && (*it)->get_x() < Hx){
				result = a*(*it)->get_x() +b;
				if(result >= (*it)->get_y() && result <= (*it)->get_yy())
					return false;
				result = a*(*it)->get_xx() +b;
				if(result >= (*it)->get_y() && result <= (*it)->get_yy())
					return false;
			}	
		}	
			
		}else return false;
	}
	else{
		if(Hy >= f1(Hx) && Hy <= f2(Hx) && Hx >= (Px-SIGHT)){
		//sprawdzenie czy nie ma przeszkod na grodze wzroku
		list<object*>::const_iterator it = colision_makers.begin();
		for(it;it!=colision_makers.end();it++){
			if((*it)->is() == BARRIER && (*it)->get_x() < Px && (*it)->get_x() > Hx){
				result = a*(*it)->get_x() +b; 
				if(result >= (*it)->get_y() && result <= (*it)->get_yy())return false;
				result = a*(*it)->get_xx() +b;
				if(result >= (*it)->get_y() && result <= (*it)->get_yy())
					return false;
			}	
		}
		}else return false;
	}

	return true;
}

void policeman::take_action(){
	if(did_my_job && fabs(get_yy() - player->get_yy()) < HIT_BORDER){
		throw_billy();
		return;
	}else if(player->get_yy() < get_yy() && my_colisions->has_with(BARRIER,TOP)){
		 vvct = POLICEMAN_JUMP;
		 return;
	}
	else if(!did_my_job && !action) action = -20;
	else return;
	
	player->give_score(150);
	did_my_job = true;
	policeman* sir = new policeman(player->get_x(),0,true);
	
	colision_makers.push_back(sir);
	colisioning.push_back(sir);
	dynamics.push_front(sir);
	
}

void policeman::throw_billy(){
	if(reloading) return;
	reloading = 50; 
	action = 20;
	
	billy* nb = new billy(x+8+direction*40,y,direction);
	colision_makers.push_back(nb);
	colisioning.push_back(nb);
	dynamics.push_front(nb);

}

void policeman::find_colisions(){
	list<object*>::const_iterator it = colision_makers.begin();
	for(it;it!=colision_makers.end();it++)
		if((*it != this ) && (*it)->is() != ENEMY)
			my_colisions->calculate(*it);
}
