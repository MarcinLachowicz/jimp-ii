/*
 * main.cpp
 *
 *  Created on: 10-03-2013
 *      Author: marcin
 */

#include <iostream>

#include "headers/Pyramid.h"

using namespace std;

int main(int argc, char **argv) {

	Pyramid firstPiramid(6);

	firstPiramid.getStorey(0)->setType(BLUE_TYPE);
	firstPiramid.getStorey(1)->setType(BLUE_TYPE);
	firstPiramid.getStorey(2)->setType(PURPLE_TYPE);
	firstPiramid.getStorey(3)->setType(PURPLE_TYPE);
	firstPiramid.getStorey(4)->setType(PURPLE_TYPE);
	firstPiramid.getStorey(5)->setType(BLUE_TYPE);

	firstPiramid.getStorey(0)->getBrick(0)->presetContent(2);
	firstPiramid.getStorey(3)->getBrick(0)->presetContent(5);
	firstPiramid.getStorey(3)->getBrick(3)->presetContent(4);
	firstPiramid.getStorey(4)->getBrick(2)->presetContent(8);
	firstPiramid.getStorey(5)->getBrick(1)->presetContent(1);
	firstPiramid.getStorey(5)->getBrick(4)->presetContent(6);
	firstPiramid.solve();
	firstPiramid.print();
	return 0;
}

