/*
 * UmowaDzielo.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef UMOWADZIELO_H_
#define UMOWADZIELO_H_

#include "Umowa.h"

class UmowaDzielo : public Umowa {
	public:
		UmowaDzielo(double pensja);
		virtual double pobierzNetto();
		virtual ~UmowaDzielo();
	private:
		float czescNetto;
};

#endif /* UMOWADZIELO_H_ */
