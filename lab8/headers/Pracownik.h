/*
 * Pracownik.h
 *
 *  Created on: 19-03-2013
 *      Author: marcin
 */

#ifndef PRACOWNIK_H_
#define PRACOWNIK_H_

#include <iostream>
#include <string>
#include "Umowa.h"

using namespace std;

class Pracownik {
	public:
	private:
		string imie, nazwisko, pesel;
		Umowa* umowa;
	public:
		Pracownik(string , string , string , Umowa*);
		Pracownik(const Pracownik&);
		virtual ~Pracownik();
		double pobierzPensje();
		friend ostream& operator<<(ostream&, Pracownik&);

};

#endif /* PRACOWNIK_H_ */
